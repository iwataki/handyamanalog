<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Accent_neu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="NXP" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="NXP_2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="KASTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="KASTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="FRNTTEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="BACKMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="BIFRNTTEK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="BIFRNTMAT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="BottomExtra" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="IC_Robotech">
<description>&lt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="TSSOP8">
<description>&lt;b&gt;Thin Shrink Small Outline Package&lt;/b&gt;&lt;p&gt;
package type ST</description>
<wire x1="1.4" y1="-2.15" x2="1.4" y2="2.15" width="0.2032" layer="21"/>
<wire x1="1.4" y1="2.15" x2="-1.4" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="2.15" x2="-1.4" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-2.15" x2="1.4" y2="-2.15" width="0.2032" layer="21"/>
<smd name="1" x="-0.975" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="2" x="-0.325" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="3" x="0.325" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="4" x="0.975" y="-2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="5" x="0.975" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="7" x="-0.325" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<smd name="8" x="-0.975" y="2.925" dx="0.35" dy="1.2" layer="1"/>
<text x="-1.625" y="-2.925" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.925" y="-3.25" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1" y1="-3.2" x2="-0.85" y2="-2.2" layer="51"/>
<rectangle x1="-0.45" y1="-3.2" x2="-0.2" y2="-2.2" layer="51"/>
<rectangle x1="0.2" y1="-3.2" x2="0.45" y2="-2.2" layer="51"/>
<rectangle x1="0.85" y1="-3.2" x2="1.1" y2="-2.2" layer="51"/>
<rectangle x1="0.85" y1="2.2" x2="1.1" y2="3.2" layer="51"/>
<rectangle x1="0.2" y1="2.2" x2="0.45" y2="3.2" layer="51"/>
<rectangle x1="-0.45" y1="2.2" x2="-0.2" y2="3.2" layer="51"/>
<rectangle x1="-1.1" y1="2.2" x2="-0.85" y2="3.2" layer="51"/>
<circle x="-0.65" y="-1.625" radius="0.325" width="0" layer="21"/>
</package>
<package name="TSSOP14">
<smd name="1" x="-2.54" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="2" x="-1.89" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="3" x="-1.24" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="4" x="-0.59" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="5" x="0.06" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="6" x="0.71" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="7" x="1.36" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="8" x="1.36" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="9" x="0.71" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="10" x="0.06" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="11" x="-0.59" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="12" x="-1.24" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<wire x1="-3.09" y1="-2.19" x2="2.052" y2="-2.19" width="0.127" layer="21"/>
<wire x1="2.052" y1="-2.19" x2="2.052" y2="2.21" width="0.127" layer="21"/>
<wire x1="2.052" y1="2.21" x2="-3.09" y2="2.21" width="0.127" layer="21"/>
<wire x1="-3.09" y1="2.21" x2="-3.09" y2="0.485" width="0.127" layer="21"/>
<circle x="-2.2225" y="-1.27" radius="0.449" width="0.127" layer="21"/>
<wire x1="-3.09" y1="0.485" x2="-3.09" y2="-0.615" width="0.127" layer="21"/>
<wire x1="-3.09" y1="-0.615" x2="-3.09" y2="-2.19" width="0.127" layer="21"/>
<wire x1="-3.09" y1="0.485" x2="-3.09" y2="-0.615" width="0.127" layer="21" curve="-180"/>
<wire x1="-3.4925" y1="-3.4925" x2="2.6185" y2="-3.4925" width="0.127" layer="51"/>
<wire x1="2.6185" y1="-3.4925" x2="2.6185" y2="3.4925" width="0.127" layer="51"/>
<wire x1="2.6185" y1="3.4925" x2="-3.4925" y2="3.4925" width="0.127" layer="51"/>
<wire x1="-3.4925" y1="3.4925" x2="-3.4925" y2="-3.4925" width="0.127" layer="51"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.4925" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-4.445" y="-3.4925" size="1.27" layer="51">1</text>
<smd name="13" x="-1.89" y="2.54" dx="0.35" dy="1.5" layer="1"/>
<smd name="14" x="-2.54" y="2.54" dx="0.35" dy="1.5" layer="1"/>
</package>
<package name="SIP9-P-2.54A" urn="urn:adsk.eagle:footprint:16365/1" locally_modified="yes">
<description>&lt;b&gt;SIP9-P-2.54A&lt;/b&gt;&lt;p&gt;
Source: THOSHIBA .. TA7358P.pdf</description>
<wire x1="11.54" y1="1.5" x2="11.54" y2="0" width="0.2032" layer="21"/>
<wire x1="11.29" y1="-1.5" x2="-10.785" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-10.785" y1="-1.475" x2="-11.035" y2="0" width="0.2032" layer="21"/>
<wire x1="-11.035" y1="0" x2="-11.035" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-11.035" y1="1.5" x2="11.54" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-10.785" y1="-1.475" x2="-10.785" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="11.54" y1="0" x2="11.29" y2="-1.5" width="0.2032" layer="21"/>
<pad name="1" x="-10.16" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="2" x="-7.62" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="3" x="-5.08" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="4" x="-2.54" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="5" x="0" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="6" x="2.54" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="7" x="5.08" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<pad name="9" x="10.16" y="0" drill="0.8" diameter="1.27" shape="long" rot="R90"/>
<text x="-10.795" y="-3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
</package>
<package name="P-HSIP3_2.30A">
<smd name="1" x="-2.54" y="0" dx="1.016" dy="1.524" layer="1" rot="R180"/>
<smd name="2" x="2.54" y="0" dx="1.016" dy="1.524" layer="1" rot="R180"/>
<smd name="3" x="0" y="6.35" dx="5.08" dy="5.08" layer="1"/>
<wire x1="3.175" y1="1.27" x2="3.175" y2="6.985" width="0.127" layer="21"/>
<wire x1="3.175" y1="6.985" x2="-3.175" y2="6.985" width="0.127" layer="21"/>
<wire x1="-3.175" y1="6.985" x2="-3.175" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="3.175" y2="1.27" width="0.127" layer="21"/>
</package>
<package name="P-HSIP3-2.30B">
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="3" x="0" y="0" drill="0.8"/>
<pad name="2" x="2.54" y="0" drill="0.8"/>
<wire x1="-3.81" y1="1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="7805-LARGEHOLE">
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="2.032" shape="long" rot="R90"/>
<wire x1="-3.81" y1="1.27" x2="-3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="3.175" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SC62">
<description>SC-62 (SOT89)</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<smd name="2-PAD" x="0" y="1.575" dx="2.032" dy="1.27" layer="1"/>
<rectangle x1="-0.89" y1="1.19" x2="0.89" y2="2.11" layer="51"/>
<wire x1="-0.7112" y1="-0.4064" x2="-0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="-0.7112" y1="0.9144" x2="0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="0.9144" x2="0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="-0.4064" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="-0.4064" x2="-0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.2032" y2="0.508" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="0.508" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0" y1="-1.4224" x2="0" y2="-0.7112" width="0.6096" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="NJM2594">
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<pin name="V+" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="OUT1" x="-17.78" y="2.54" length="middle" direction="out"/>
<pin name="OUT2" x="-17.78" y="-2.54" length="middle" direction="out"/>
<pin name="GND" x="-17.78" y="-7.62" length="middle" direction="pwr"/>
<pin name="SIGNAL" x="17.78" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="BYPASS" x="17.78" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="CARRIER" x="17.78" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="NC" x="17.78" y="7.62" length="middle" direction="nc" rot="R180"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.7" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="NJM2783">
<pin name="GAIN2" x="-15.24" y="5.08" visible="pin" length="middle" direction="out"/>
<pin name="GAIN1" x="-15.24" y="7.62" visible="pin" length="middle" direction="in"/>
<pin name="DET" x="-15.24" y="2.54" visible="pin" length="middle" direction="in"/>
<pin name="OUT" x="-15.24" y="0" visible="pin" length="middle" direction="out"/>
<pin name="BUFFIN" x="-15.24" y="-2.54" visible="pin" length="middle" direction="in"/>
<pin name="BUFFGAIN" x="-15.24" y="-5.08" visible="pin" length="middle"/>
<pin name="BUFFOUT" x="-15.24" y="-7.62" visible="pin" length="middle" direction="out"/>
<pin name="GND" x="15.24" y="-7.62" visible="pin" length="middle" direction="sup" rot="R180"/>
<pin name="V+" x="15.24" y="-5.08" visible="pin" length="middle" direction="sup" rot="R180"/>
<pin name="VREF" x="15.24" y="-2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="SENSE" x="15.24" y="0" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="INT" x="15.24" y="2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="GAINSW" x="15.24" y="5.08" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="IN" x="15.24" y="7.62" visible="pin" length="middle" direction="in" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LA1600">
<pin name="RFIN" x="-10.16" y="-2.54" length="point" rot="R90"/>
<pin name="RFBIAS" x="-7.62" y="-2.54" length="point" rot="R90"/>
<pin name="OSC" x="-5.08" y="-2.54" length="point" rot="R90"/>
<pin name="MIXOUT" x="-2.54" y="-2.54" length="point" rot="R90"/>
<pin name="GND" x="0" y="-2.54" length="point" rot="R90"/>
<pin name="VAGC" x="2.54" y="-2.54" length="point" rot="R90"/>
<pin name="IFIN" x="5.08" y="-2.54" length="point" rot="R90"/>
<pin name="VCC" x="7.62" y="-2.54" length="point" rot="R90"/>
<pin name="AFOUT" x="10.16" y="-2.54" length="point" rot="R90"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="-12.7" y2="0" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP">
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="PWR+-">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="1.27" y="3.175" size="0.8128" layer="93" rot="R90">V+</text>
<text x="1.27" y="-4.445" size="0.8128" layer="93" rot="R90">V-</text>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="78**">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<pin name="I" x="-7.62" y="2.54" length="short" direction="pwr"/>
<pin name="O" x="7.62" y="2.54" length="short" direction="sup" rot="R180"/>
<pin name="G" x="0" y="-2.54" length="short" direction="pwr" rot="R90"/>
<text x="-5.08" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NJM2594" prefix="IC">
<gates>
<gate name="G$1" symbol="NJM2594" x="0" y="0"/>
</gates>
<devices>
<device name="V" package="TSSOP8">
<connects>
<connect gate="G$1" pin="BYPASS" pad="6"/>
<connect gate="G$1" pin="CARRIER" pad="7"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="NC" pad="8"/>
<connect gate="G$1" pin="OUT1" pad="2"/>
<connect gate="G$1" pin="OUT2" pad="3"/>
<connect gate="G$1" pin="SIGNAL" pad="5"/>
<connect gate="G$1" pin="V+" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NJM2783" prefix="IC">
<gates>
<gate name="A" symbol="NJM2783" x="0" y="0"/>
</gates>
<devices>
<device name="V" package="TSSOP14">
<connects>
<connect gate="A" pin="BUFFGAIN" pad="6"/>
<connect gate="A" pin="BUFFIN" pad="5"/>
<connect gate="A" pin="BUFFOUT" pad="7"/>
<connect gate="A" pin="DET" pad="3"/>
<connect gate="A" pin="GAIN1" pad="1"/>
<connect gate="A" pin="GAIN2" pad="2"/>
<connect gate="A" pin="GAINSW" pad="13"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="IN" pad="14"/>
<connect gate="A" pin="INT" pad="12"/>
<connect gate="A" pin="OUT" pad="4"/>
<connect gate="A" pin="SENSE" pad="11"/>
<connect gate="A" pin="V+" pad="9"/>
<connect gate="A" pin="VREF" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LA1600" prefix="IC">
<gates>
<gate name="G$1" symbol="LA1600" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SIP9-P-2.54A">
<connects>
<connect gate="G$1" pin="AFOUT" pad="9"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="IFIN" pad="7"/>
<connect gate="G$1" pin="MIXOUT" pad="4"/>
<connect gate="G$1" pin="OSC" pad="3"/>
<connect gate="G$1" pin="RFBIAS" pad="2"/>
<connect gate="G$1" pin="RFIN" pad="1"/>
<connect gate="G$1" pin="VAGC" pad="6"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMC662">
<gates>
<gate name="A" symbol="OPAMP" x="12.7" y="7.62"/>
<gate name="B" symbol="OPAMP" x="12.7" y="-7.62"/>
<gate name="P" symbol="PWR+-" x="12.7" y="7.62"/>
</gates>
<devices>
<device name="" package="DIL08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="78*" prefix="IC">
<description>&lt;b&gt;Three Terminal Voltage Regulator&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="78**" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-HSIP3_2.30A">
<connects>
<connect gate="G$1" pin="G" pad="3"/>
<connect gate="G$1" pin="I" pad="1"/>
<connect gate="G$1" pin="O" pad="2"/>
</connects>
<technologies>
<technology name=""/>
<technology name="033"/>
<technology name="05"/>
</technologies>
</device>
<device name="(S)" package="P-HSIP3-2.30B">
<connects>
<connect gate="G$1" pin="G" pad="3"/>
<connect gate="G$1" pin="I" pad="1"/>
<connect gate="G$1" pin="O" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="(LARGEHOLE)" package="7805-LARGEHOLE">
<connects>
<connect gate="G$1" pin="G" pad="3"/>
<connect gate="G$1" pin="I" pad="1"/>
<connect gate="G$1" pin="O" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TA78L*F" prefix="IC">
<description>&lt;b&gt;Voltage Regulator&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="78**" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SC62">
<connects>
<connect gate="G$1" pin="G" pad="2"/>
<connect gate="G$1" pin="I" pad="3"/>
<connect gate="G$1" pin="O" pad="1"/>
</connects>
<technologies>
<technology name="05">
<attribute name="VOUT" value="5V" constant="no"/>
</technology>
<technology name="06">
<attribute name="VOUT" value="6V" constant="no"/>
</technology>
<technology name="07">
<attribute name="VOUT" value="7V" constant="no"/>
</technology>
<technology name="08">
<attribute name="VOUT" value="8V" constant="no"/>
</technology>
<technology name="09">
<attribute name="VOUT" value="9V" constant="no"/>
</technology>
<technology name="10">
<attribute name="VOUT" value="10V" constant="no"/>
</technology>
<technology name="12">
<attribute name="VOUT" value="12V" constant="no"/>
</technology>
<technology name="15">
<attribute name="VOUT" value="15V" constant="no"/>
</technology>
<technology name="18">
<attribute name="VOUT" value="18V" constant="no"/>
</technology>
<technology name="20">
<attribute name="VOUT" value="20V" constant="no"/>
</technology>
<technology name="24">
<attribute name="VOUT" value="24V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-power">
<description>&lt;b&gt;Power Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="RD06HVF1">
<description>&lt;b&gt;RD06HVF1&lt;/b&gt;&lt;p&gt;
Source: MITSUBISHI .. rd06hvf1.pdf</description>
<wire x1="4.299" y1="-4.318" x2="4.478" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.299" y1="-4.318" x2="-4.224" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.478" y1="-4.064" x2="-4.224" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="4.655" y1="-1.218" x2="4.478" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-4.478" y1="-4.064" x2="-4.655" y2="-1.243" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1" diameter="1.3" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1" diameter="1.3" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1" diameter="1.3" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.75" y1="-0.762" x2="4.75" y2="0" layer="21"/>
<rectangle x1="-4.75" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="4.75" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
<package name="TO220AH">
<description>&lt;b&gt;Molded Package&lt;/b&gt;&lt;p&gt;
grid 2.54 mm</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.1524" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.1524" layer="21"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="42"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="43"/>
<pad name="B" x="-2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="C" x="0" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="E" x="2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<text x="-3.81" y="5.207" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="1.016" layer="21" ratio="10">A17,5mm</text>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-4.064" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-4.064" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-4.064" layer="21"/>
<rectangle x1="-3.175" y1="-4.064" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-4.064" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-4.064" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-2.921" y1="-6.604" x2="-2.159" y2="-4.699" layer="51"/>
<rectangle x1="-0.381" y1="-6.604" x2="0.381" y2="-4.699" layer="51"/>
<rectangle x1="2.159" y1="-6.604" x2="2.921" y2="-4.699" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
</package>
</packages>
<symbols>
<symbol name="MFN">
<wire x1="-1.1176" y1="2.413" x2="-1.1176" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.1176" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="0.5334" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="0" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.778" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.508" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0.254" x2="0.762" y2="0" width="0.3048" layer="94"/>
<wire x1="0.762" y1="0" x2="1.651" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="1.651" y1="-0.254" x2="1.651" y2="0" width="0.3048" layer="94"/>
<wire x1="1.651" y1="0" x2="1.397" y2="0" width="0.3048" layer="94"/>
<circle x="2.54" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="2.54" size="0.8128" layer="93">D</text>
<text x="1.27" y="-3.175" size="0.8128" layer="93">S</text>
<text x="-2.54" y="-1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="-1.27" layer="94"/>
<rectangle x1="-0.254" y1="1.27" x2="0.508" y2="2.54" layer="94"/>
<rectangle x1="-0.254" y1="-0.889" x2="0.508" y2="0.889" layer="94"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RD06HVF1" prefix="Q">
<description>&lt;b&gt;RF Power MOSFET Transistor 175MHz,6W&lt;/b&gt;&lt;p&gt;
Source: MITSUBISHI .. rd06hvf1.pdf</description>
<gates>
<gate name="G$1" symbol="MFN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RD06HVF1">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="H" package="TO220AH">
<connects>
<connect gate="G$1" pin="D" pad="E"/>
<connect gate="G$1" pin="G" pad="B"/>
<connect gate="G$1" pin="S" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="FCZ-7MM">
<pad name="1" x="-2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="2" x="-2.5" y="0" drill="0.8" shape="octagon"/>
<pad name="3" x="-2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="6" x="2.5" y="-2.5" drill="0.8" shape="octagon"/>
<pad name="5" x="2.5" y="0" drill="0.8" shape="octagon"/>
<pad name="4" x="2.5" y="2.5" drill="0.8" shape="octagon"/>
<pad name="7" x="0" y="3.81" drill="1.5" shape="octagon"/>
<pad name="8" x="0" y="-3.81" drill="1.5" shape="octagon"/>
<wire x1="-3.81" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<text x="-2.54" y="-6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="FCZ-10MM">
<pad name="1S" x="-3.5" y="3.5" drill="0.8" shape="octagon"/>
<pad name="1C" x="-3.5" y="0" drill="0.8" shape="octagon"/>
<pad name="1E" x="-3.5" y="-3.5" drill="0.8" shape="octagon"/>
<pad name="2S" x="3.5" y="3.5" drill="0.8" shape="octagon"/>
<pad name="2E" x="3.5" y="-3.5" drill="0.8" shape="octagon"/>
<wire x1="-5" y1="5" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="-5" y2="5" width="0.127" layer="21"/>
<pad name="P$6" x="0" y="5" drill="1.6" shape="octagon"/>
<pad name="P$7" x="0" y="-5" drill="1.6" shape="octagon"/>
<text x="-2.54" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="VR_SM">
<text x="-1.905" y="4.445" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.905" y="-5.715" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
<smd name="2" x="0" y="1.524" dx="2.1844" dy="1.0668" layer="1" rot="R90"/>
<smd name="1" x="-1.016" y="-1.524" dx="2.1844" dy="1.0668" layer="1" rot="R90"/>
<smd name="3" x="1.016" y="-1.524" dx="2.1844" dy="1.0668" layer="1" rot="R90"/>
</package>
<package name="VR-637A">
<pad name="1" x="-2.54" y="-2.54" drill="1.2" shape="octagon"/>
<pad name="2" x="2.54" y="-2.54" drill="1.2" shape="octagon"/>
<pad name="BR" x="0" y="2.54" drill="1.2" shape="octagon"/>
<wire x1="-3.2" y1="-3.65" x2="3.2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="3.2" y1="-3.65" x2="3.2" y2="1.11" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.11" x2="0.66" y2="3.65" width="0.127" layer="21" curve="90"/>
<wire x1="0.66" y1="3.65" x2="-0.66" y2="3.65" width="0.127" layer="21"/>
<wire x1="-0.66" y1="3.65" x2="-3.2" y2="1.11" width="0.127" layer="21" curve="90"/>
<wire x1="-3.2" y1="1.11" x2="-3.2" y2="-3.65" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.127" layer="21"/>
</package>
<package name="ST-EB">
<smd name="3" x="-1.175" y="-3" dx="1.6" dy="2" layer="1"/>
<smd name="1" x="1.175" y="-3" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="0" y="3" dx="2" dy="2" layer="1"/>
<wire x1="-2.5" y1="-2.25" x2="2.5" y2="-2.25" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.25" x2="2.5" y2="2.25" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.25" x2="-2.5" y2="2.25" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.25" x2="-2.5" y2="-2.25" width="0.127" layer="21"/>
<text x="2.54" y="1.27" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="VR2">
<pad name="1" x="-2.54" y="0" drill="1.2" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.2" shape="octagon"/>
<pad name="BR" x="0" y="2.54" drill="1.2" shape="octagon"/>
<wire x1="-3.2" y1="-3.65" x2="3.2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="3.2" y1="-3.65" x2="3.2" y2="1.11" width="0.127" layer="21"/>
<wire x1="3.2" y1="1.11" x2="0.66" y2="3.65" width="0.127" layer="21" curve="90"/>
<wire x1="0.66" y1="3.65" x2="-0.66" y2="3.65" width="0.127" layer="21"/>
<wire x1="-0.66" y1="3.65" x2="-3.2" y2="1.11" width="0.127" layer="21" curve="90"/>
<wire x1="-3.2" y1="1.11" x2="-3.2" y2="-3.65" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="2.54" y2="0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="-0.635" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.127" layer="21"/>
</package>
<package name="CFULA">
<wire x1="-4" y1="3.5" x2="4" y2="3.5" width="0.127" layer="21"/>
<wire x1="4" y1="3.5" x2="4" y2="-3.5" width="0.127" layer="21"/>
<wire x1="4" y1="-3.5" x2="-3.81" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.5" x2="-4" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-3.5" x2="-4" y2="3.5" width="0.127" layer="21"/>
<pad name="3" x="-0.7" y="2.1" drill="1"/>
<pad name="2" x="0.1" y="-2.1" drill="1"/>
<pad name="1" x="2.8" y="-2.1" drill="1"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.7707" cap="flat"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.7719" cap="flat"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.8427" cap="flat"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.8427" cap="flat"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.7714" cap="flat"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.315" cap="flat"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.4562" cap="flat"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.7719" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.315" cap="flat"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.8427" cap="flat"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.8427" cap="flat"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.4562" cap="flat"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="TINYCRYSTAL">
<description>Tiny 32768Hz crystal</description>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="5.08" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="5.08" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-1.27" y1="-2" x2="1.27" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.23" y1="0" x2="-1.23" y2="-2" width="0.3048" layer="51"/>
<wire x1="1.23" y1="0" x2="1.23" y2="-2" width="0.3048" layer="51"/>
<wire x1="-1.27" y1="-2" x2="-1.27" y2="-8" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-8" x2="1.27" y2="-8" width="0.127" layer="21"/>
<wire x1="1.27" y1="-8" x2="1.27" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
<package name="NR6028T">
<description>&lt;b&gt;NR6028&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.35" y="0" dx="5.7" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2" layer="51"/>
<wire x1="-4.15" y1="4" x2="4.15" y2="4" width="0.1" layer="51"/>
<wire x1="4.15" y1="4" x2="4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="4.15" y1="-4" x2="-4.15" y2="-4" width="0.1" layer="51"/>
<wire x1="-4.15" y1="-4" x2="-4.15" y2="4" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.1" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="21"/>
</package>
<package name="NR8040T220M">
<description>&lt;b&gt;NR8040&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<smd name="2" x="2.8" y="0" dx="7.5" dy="1.8" layer="1" rot="R90"/>
<text x="-1.019" y="0.137" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-1.019" y="0.137" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4" y1="4" x2="4" y2="4" width="0.2" layer="51"/>
<wire x1="4" y1="4" x2="4" y2="-4" width="0.2" layer="51"/>
<wire x1="4" y1="-4" x2="-4" y2="-4" width="0.2" layer="51"/>
<wire x1="-4" y1="-4" x2="-4" y2="4" width="0.2" layer="51"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.2" layer="21"/>
<wire x1="-4" y1="-4" x2="4" y2="-4" width="0.2" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.2" layer="21"/>
<wire x1="4" y1="4" x2="-4" y2="4" width="0.2" layer="21"/>
</package>
<package name="153CLV-0405">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<smd name="-" x="-1.8" y="0" dx="2.6" dy="1.6" layer="1"/>
<wire x1="1.25" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-1.1" x2="-2.15" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="2.15" x2="1.25" y2="2.15" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.25" x2="2.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.15" y1="1.1" x2="2.15" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.05" x2="2.15" y2="-1.25" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.25" x2="1.25" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="1.25" y1="2.15" x2="2.15" y2="1.25" width="0.2032" layer="21"/>
<text x="-2.24" y="2.48" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.275" y="-3.735" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.65" y1="-0.95" x2="1.65" y2="-0.95" width="0.2032" layer="21" curve="120.137" cap="flat"/>
<wire x1="-1.65" y1="-0.95" x2="-1.65" y2="0.95" width="0.2032" layer="51" curve="-59.863" cap="flat"/>
<wire x1="-1.65" y1="0.95" x2="1.65" y2="0.95" width="0.2032" layer="21" curve="-120.137" cap="flat"/>
<wire x1="1.65" y1="-0.95" x2="1.65" y2="0.95" width="0.2032" layer="51" curve="59.863" cap="flat"/>
</package>
<package name="153CLV-0505">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="2.2" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="-" x="-2.2" y="0" dx="3" dy="1.6" layer="1"/>
<wire x1="1.75" y1="-2.65" x2="-2.65" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="-2.65" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-1.1" x2="-2.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-2.65" y1="1.1" x2="-2.65" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="2.65" x2="1.75" y2="2.65" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.75" x2="2.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.1" x2="2.65" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="2.65" y1="-1.1" x2="2.65" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.75" x2="1.75" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.75" y1="2.65" x2="2.65" y2="1.75" width="0.2032" layer="21"/>
<text x="-2.74" y="2.98" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.775" y="-4.235" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.2" y1="-0.95" x2="2.2" y2="-0.95" width="0.2032" layer="21" curve="133.289" cap="flat"/>
<wire x1="-2.2" y1="-0.95" x2="-2.2" y2="0.95" width="0.2032" layer="51" curve="-46.7111" cap="flat"/>
<wire x1="-2.2" y1="0.95" x2="2.2" y2="0.95" width="0.2032" layer="21" curve="-133.289" cap="flat"/>
<wire x1="2.2" y1="-0.95" x2="2.2" y2="0.95" width="0.2032" layer="51" curve="46.7111" cap="flat"/>
</package>
<package name="153CLV-0605">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="-" x="-2.7" y="0" dx="3.5" dy="1.6" layer="1"/>
<wire x1="2.4" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.1" x2="-3.3" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="1.1" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="2.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.1" x2="3.3" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-1.1" x2="3.3" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.4" x2="2.4" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.4" y1="3.3" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<text x="-3.39" y="3.63" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.425" y="-4.885" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.95" y1="-0.9" x2="2.95" y2="-0.95" width="0.2032" layer="21" curve="145.181" cap="flat"/>
<wire x1="-2.95" y1="-0.9" x2="-2.95" y2="0.95" width="0.2032" layer="51" curve="-34.8186" cap="flat"/>
<wire x1="-2.95" y1="0.95" x2="2.95" y2="0.9" width="0.2032" layer="21" curve="-145.181" cap="flat"/>
<wire x1="2.95" y1="-0.95" x2="2.95" y2="0.9" width="0.2032" layer="51" curve="34.8186" cap="flat"/>
</package>
<package name="153CLV-0807">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="3.05" y="0" dx="4" dy="1.6" layer="1"/>
<smd name="-" x="-3.05" y="0" dx="4" dy="1.6" layer="1"/>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.1" x2="-4.2" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.1" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.1" x2="4.2" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.1" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<text x="-4.29" y="4.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-4.325" y="-5.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-3.75" y1="-1.05" x2="3.75" y2="-1.05" width="0.2032" layer="21" curve="148.008" cap="flat"/>
<wire x1="-3.75" y1="-1.05" x2="-3.75" y2="1.05" width="0.2032" layer="51" curve="-31.2845" cap="flat"/>
<wire x1="-3.75" y1="1.05" x2="3.75" y2="1.05" width="0.2032" layer="21" curve="-148.008" cap="flat"/>
<wire x1="3.75" y1="1.05" x2="3.75" y2="-1.05" width="0.2032" layer="51" curve="-31.2845" cap="flat"/>
</package>
<package name="153CLV-0810">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="3.25" y="0" dx="3.5" dy="2.5" layer="1"/>
<smd name="-" x="-3.25" y="0" dx="3.5" dy="2.5" layer="1"/>
<wire x1="3.3" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.5" x2="-4.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-4.2" y1="1.5" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="3.3" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3.3" x2="4.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.5" x2="4.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-1.5" x2="4.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-3.3" x2="3.3" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="4.2" x2="4.2" y2="3.3" width="0.2032" layer="21"/>
<text x="-4.29" y="4.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-4.325" y="-5.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-3.65" y1="-1.4" x2="3.65" y2="-1.4" width="0.2032" layer="21" curve="138.03" cap="flat"/>
<wire x1="-3.65" y1="-1.4" x2="-3.65" y2="1.4" width="0.2032" layer="51" curve="-41.9698" cap="flat"/>
<wire x1="-3.65" y1="1.4" x2="3.65" y2="1.4" width="0.2032" layer="21" curve="-138.03" cap="flat"/>
<wire x1="3.65" y1="-1.4" x2="3.65" y2="1.4" width="0.2032" layer="51" curve="41.9698" cap="flat"/>
</package>
<package name="153CLV-1010">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4" y="0" dx="4" dy="2.5" layer="1"/>
<smd name="-" x="-4" y="0" dx="4" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="153CLV-1012">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="153CLV-1014">
<description>&lt;b&gt;Aluminum electrolytic capacitors&lt;/b&gt;&lt;p&gt;
SMD (Chip) Long Life Vertical 153 CLV&lt;p&gt;
http://www.bccomponents.com/</description>
<smd name="+" x="4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<smd name="-" x="-4.15" y="0" dx="4.3" dy="2.5" layer="1"/>
<wire x1="4.3" y1="-5.2" x2="-5.2" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-5.2" x2="-5.2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="-1.5" x2="-5.2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-5.2" y1="1.5" x2="-5.2" y2="5.2" width="0.2032" layer="21"/>
<wire x1="-5.2" y1="5.2" x2="4.3" y2="5.2" width="0.2032" layer="21"/>
<wire x1="5.2" y1="4.3" x2="5.2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.2" y1="1.5" x2="5.2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="5.2" y1="-1.5" x2="5.2" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="5.2" y1="-4.3" x2="4.3" y2="-5.2" width="0.2032" layer="21"/>
<wire x1="4.3" y1="5.2" x2="5.2" y2="4.3" width="0.2032" layer="21"/>
<text x="-5.29" y="5.53" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-5.325" y="-6.785" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<wire x1="4.65" y1="-1.55" x2="4.65" y2="1.55" width="0.2032" layer="51" curve="36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="-4.65" y2="1.55" width="0.2032" layer="51" curve="-36.8699" cap="flat"/>
<wire x1="-4.65" y1="-1.55" x2="4.65" y2="-1.55" width="0.2032" layer="21" curve="143.13" cap="flat"/>
<wire x1="-4.65" y1="1.55" x2="4.65" y2="1.55" width="0.2032" layer="21" curve="-143.13" cap="flat"/>
</package>
<package name="E2,5-5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 5 mm</description>
<text x="2.413" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.413" y="-2.413" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<wire x1="-1.651" y1="1.27" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="-1.397" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.143" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.27" x2="-1.397" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
</package>
<package name="E2,5-6">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 6 mm</description>
<text x="2.667" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.667" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<circle x="0" y="0" radius="2.794" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-1.651" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0.889" x2="-1.651" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="1.27" x2="-1.651" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E2,5-7">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, diameter 7 mm</description>
<text x="3.048" y="2.032" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="3.048" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="-" x="1.27" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="+" x="-1.27" y="0" drill="0.8128" diameter="1.6002"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="-0.381" x2="-2.667" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.635" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<rectangle x1="0.254" y1="-1.27" x2="0.762" y2="1.27" layer="51"/>
</package>
<package name="E5-10,5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 10.5 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="4.699" y="2.7432" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.1242" y="-3.2258" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="5.08" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-13">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 13 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-0.889" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.27" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="6.3754" y="4.1148" size="1.778" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.572" y="-3.937" size="1.778" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="6.985" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.445" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.27" x2="0.889" y2="1.27" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 5 mm</description>
<pad name="+" x="-2.54" y="0" drill="0.8128" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8128" diameter="1.905" shape="octagon"/>
<wire x1="-2.1557" y1="1.3432" x2="2.1557" y2="1.3432" width="0.1524" layer="21" curve="-116.147" cap="flat"/>
<wire x1="-2.1557" y1="-1.3432" x2="2.1557" y2="-1.3432" width="0.1524" layer="21" curve="116.147" cap="flat"/>
<wire x1="-2.1557" y1="1.3432" x2="-2.1557" y2="-1.3432" width="0.1524" layer="51" curve="63.8534" cap="flat"/>
<wire x1="2.1557" y1="-1.3432" x2="2.1557" y2="1.3432" width="0.1524" layer="51" curve="63.8534" cap="flat"/>
<text x="2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="E5-6">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 6 mm</description>
<pad name="+" x="-2.54" y="0" drill="0.8128" diameter="1.905"/>
<pad name="-" x="2.54" y="0" drill="0.8128" diameter="1.905" shape="octagon"/>
<wire x1="-2.8702" y1="1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="21" curve="-129.378" cap="flat"/>
<wire x1="-2.8702" y1="-1.3574" x2="2.8702" y2="-1.3574" width="0.1524" layer="21" curve="129.378" cap="flat"/>
<wire x1="-2.8702" y1="1.3574" x2="-2.8702" y2="-1.3574" width="0.1524" layer="51" curve="50.6216" cap="flat"/>
<wire x1="2.8702" y1="-1.3574" x2="2.8702" y2="1.3574" width="0.1524" layer="51" curve="50.6216" cap="flat"/>
<text x="3.048" y="1.778" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="3.048" y="-2.921" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.397" y1="0" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="-0.254" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="-0.762" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.016" x2="-0.762" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.016" x2="0.762" y2="1.016" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
</package>
<package name="E5-8,5">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 8.5 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="4.1402" y="2.286" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.5146" y="-3.0226" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="4.445" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="2.032" x2="-3.302" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.651" x2="-2.921" y2="1.651" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="E5-10,5A">
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="5.08" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="5.08" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<wire x1="-5" y1="-2" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="5" y2="-3" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="5" y2="-22" width="0.127" layer="21"/>
<wire x1="5" y1="-22" x2="-5" y2="-22" width="0.127" layer="21"/>
<wire x1="-5" y1="-22" x2="-5" y2="-5" width="0.127" layer="21"/>
<wire x1="-5" y1="-3" x2="-5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-3" x2="5" y2="-5" width="0.127" layer="21" curve="106.26"/>
<wire x1="-5" y1="-3" x2="-5" y2="-5" width="0.127" layer="21" curve="-106.26"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="-2" width="0.3048" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="-2" width="0.3048" layer="51"/>
</package>
<package name="E5-10,5A2">
<pad name="+" x="-2.54" y="0" drill="1.016" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.143" x2="-0.254" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.143" x2="-0.254" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.143" x2="-0.889" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.143" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="5.08" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="5.08" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.81" y1="1.651" x2="-3.81" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="-4.191" y2="1.27" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.143" x2="0.889" y2="1.143" layer="21"/>
<wire x1="1.143" y1="0" x2="1.651" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
<wire x1="5" y1="2" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="2" x2="-5" y2="3" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="-5" y2="22" width="0.127" layer="21"/>
<wire x1="-5" y1="22" x2="5" y2="22" width="0.127" layer="21"/>
<wire x1="5" y1="22" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="3" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="3" x2="-5" y2="5" width="0.127" layer="21" curve="106.26"/>
<wire x1="5" y1="3" x2="5" y2="5" width="0.127" layer="21" curve="-106.26"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="2" width="0.3048" layer="51"/>
<wire x1="2.5" y1="0" x2="2.5" y2="2" width="0.3048" layer="51"/>
</package>
<package name="E5-21">
<description>&lt;b&gt;ELECTROLYTIC CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5.08 mm, diameter 21 mm</description>
<pad name="+" x="-2.54" y="0" drill="1.5" diameter="2.54"/>
<pad name="-" x="2.54" y="0" drill="1.5" diameter="2.54" shape="octagon"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="0" x2="-0.889" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.27" x2="-0.254" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.27" x2="-0.889" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.27" x2="-0.889" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.1524" layer="21"/>
<text x="8.9154" y="6.6548" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="-3.937" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="10.541" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.445" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<rectangle x1="0.254" y1="-1.27" x2="0.889" y2="1.27" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0" x2="-1.143" y2="0" width="0.1524" layer="51"/>
</package>
<package name="MICROPHONE">
<circle x="0" y="0" radius="4.85" width="0.127" layer="21"/>
<pad name="-" x="-1.25" y="2" drill="0.8" shape="long" rot="R90"/>
<pad name="+" x="1.25" y="2" drill="0.8" shape="long" rot="R90"/>
<text x="-5.08" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.54" y="0" size="1.27" layer="21">+</text>
<text x="-2.54" y="0" size="1.27" layer="21">-</text>
</package>
<package name="PTR901">
<pad name="1" x="-5" y="-2.5" drill="1.2"/>
<pad name="2" x="-5" y="0" drill="1.2"/>
<pad name="3" x="-5" y="2.5" drill="1.2"/>
<pad name="S1" x="-11.25" y="-2.5" drill="1.2"/>
<pad name="S2" x="-11.25" y="2.5" drill="1.2"/>
<wire x1="0" y1="4.75" x2="0" y2="3.5" width="0.127" layer="21"/>
<wire x1="0" y1="3.5" x2="0" y2="3" width="0.127" layer="21"/>
<wire x1="0" y1="3" x2="0" y2="-3" width="0.127" layer="21"/>
<wire x1="0" y1="-3" x2="0" y2="-3.5" width="0.127" layer="21"/>
<wire x1="0" y1="-3.5" x2="0" y2="-4.76" width="0.127" layer="21"/>
<wire x1="0" y1="-4.76" x2="-13.4" y2="-4.76" width="0.127" layer="21"/>
<wire x1="-13.4" y1="-4.76" x2="-13.4" y2="4.75" width="0.127" layer="21"/>
<wire x1="-13.4" y1="4.75" x2="0" y2="4.75" width="0.127" layer="21"/>
<wire x1="0" y1="3" x2="8" y2="3" width="0.127" layer="21"/>
<wire x1="8" y1="3" x2="15" y2="3" width="0.127" layer="21"/>
<wire x1="15" y1="3" x2="15" y2="-1.27" width="0.127" layer="21"/>
<wire x1="15" y1="-1.27" x2="15" y2="-3" width="0.127" layer="21"/>
<wire x1="15" y1="-3" x2="0" y2="-3" width="0.127" layer="21"/>
<wire x1="8" y1="3" x2="8" y2="-1.27" width="0.127" layer="21"/>
<wire x1="8" y1="-1.27" x2="15" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="3.5" x2="5" y2="3.5" width="0.127" layer="21"/>
<wire x1="5" y1="3.5" x2="5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="5" y1="-3.5" x2="0" y2="-3.5" width="0.127" layer="21"/>
</package>
<package name="CTRIM3008" urn="urn:adsk.eagle:footprint:23277/1">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<pad name="+" x="-1.875" y="0" drill="1"/>
<pad name="-" x="1.875" y="0" drill="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.25" y2="0.9" layer="51"/>
<rectangle x1="2.25" y1="-1" x2="2.45" y2="1" layer="51"/>
<rectangle x1="2.45" y1="-0.5" x2="2.65" y2="0.5" layer="51"/>
</package>
<package name="CTRIM3018_11" urn="urn:adsk.eagle:footprint:23278/1">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<smd name="-" x="2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.6" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="2.6" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3018_12" urn="urn:adsk.eagle:footprint:23279/1">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-3" y="0" dx="2" dy="1.6" layer="1"/>
<smd name="-" x="3" y="0" dx="2" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-3.5" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="3.5" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3040.427" urn="urn:adsk.eagle:footprint:23280/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.35" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.35" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3040.428" urn="urn:adsk.eagle:footprint:23281/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
<hole x="0" y="0" drill="4.5"/>
</package>
<package name="CTRIM3040.448" urn="urn:adsk.eagle:footprint:23282/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.450" urn="urn:adsk.eagle:footprint:23283/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.452" urn="urn:adsk.eagle:footprint:23284/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side for automatic adjustment, vertical to PCB</description>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="-0.85" y1="1.5" x2="-1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="0" x2="-0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="-1.5" x2="0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.35" y1="0.45" x2="1.05" y2="-1" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.95" x2="1.35" y2="-0.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.4508" width="0.1016" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3050.504" urn="urn:adsk.eagle:footprint:23285/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIM3050.505" urn="urn:adsk.eagle:footprint:23286/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0.2" y="0" drill="2.5"/>
</package>
<package name="CTRIM3050.506" urn="urn:adsk.eagle:footprint:23287/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-2.63" x2="-1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="2.4" y1="-1.63" x2="2.4" y2="0.22" width="0.254" layer="21"/>
<wire x1="2.4" y1="1.67" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.2" y1="-2.63" x2="-0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.63" x2="-0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.13" x2="0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.13" x2="0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="2.4" y1="0.22" x2="2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.45" y1="1.67" x2="1.45" y2="1.67" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="1.27" drill="1.1"/>
<pad name="1B" x="2.5" y="1.27" drill="1.1"/>
<pad name="2" x="0" y="-0.23" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIMCTZ2" urn="urn:adsk.eagle:footprint:23288/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="51"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="-0.45" y1="1.35" x2="0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="0.45" y1="1.35" x2="1.15" y2="0.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="0.4" x2="1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="-0.5" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="21"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="21"/>
<wire x1="0.5" y1="-1.4" x2="1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="-1.4" x2="1.15" y2="0.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="0.4" x2="0.45" y2="1.35" width="0.254" layer="21"/>
<circle x="0" y="0" radius="0.75" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.25" dx="0.5" dy="0.45" layer="1"/>
<smd name="2" x="0" y="-1.25" dx="0.55" dy="0.5" layer="1"/>
<text x="-1.4" y="-1.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.7" y="-1.5" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.55" x2="0.15" y2="0.55" layer="21"/>
<rectangle x1="-0.55" y1="-0.15" x2="0.55" y2="0.15" layer="21"/>
</package>
<package name="CTRIMCTZ3" urn="urn:adsk.eagle:footprint:23289/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="-0.45" y1="2.1" x2="0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="0.45" y1="2.1" x2="1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.45" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-0.6" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="21"/>
<wire x1="0.6" y1="-2.15" x2="1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.45" y1="-2.15" x2="1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="0.75" x2="0.45" y2="2.1" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.1" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.95" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0" y="-1.95" dx="0.78" dy="0.6" layer="1"/>
<text x="-1.85" y="-2.3" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.1" y="-2.3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-0.85" x2="0.25" y2="0.85" layer="21"/>
<rectangle x1="-0.85" y1="-0.25" x2="0.85" y2="0.25" layer="21"/>
</package>
<package name="CTRIMTZBX4" urn="urn:adsk.eagle:footprint:23290/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="-1.9" y1="2.15" x2="-1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-1.9" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="2.15" x2="-1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="-1.05" y1="-2.15" x2="-1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="-2.15" x2="-1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="2.15" x2="-1.05" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.05" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="2.15" x2="1.05" y2="2.15" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1524" layer="51"/>
<smd name="1" x="0" y="1.8" dx="1.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.8" dx="1.6" dy="1.2" layer="1"/>
<text x="-2.3" y="-2.25" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.55" y="-2.25" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.2" x2="0.25" y2="1.2" layer="51"/>
<rectangle x1="-1.2" y1="-0.25" x2="1.2" y2="0.25" layer="51"/>
</package>
<package name="CTRIMCV05" urn="urn:adsk.eagle:footprint:23291/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; BC-Components</description>
<wire x1="-2.8" y1="0" x2="2.8" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="-2.8" y1="0" x2="-1.05" y2="2.6" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="1.05" y1="2.6" x2="2.8" y2="0" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="-2.8" y1="0" x2="-2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-2.8" y1="-2.9" x2="-1.1" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-1.1" y1="-2.9" x2="1.15" y2="-2.9" width="0.254" layer="51"/>
<wire x1="1.15" y1="-2.9" x2="2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="2.8" y1="-2.9" x2="2.8" y2="0" width="0.254" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="1.15" x2="0" y2="2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="2.25" x2="1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="1.95" y1="-1.1" x2="0" y2="-2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="-2.25" x2="-1.95" y2="-1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="1.15" x2="-0.7" y2="1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="1.85" x2="1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-0.7" y2="-1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-1.85" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-3.05" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.5" y="-3.05" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.9" y1="-0.5" x2="1.95" y2="0.5" layer="21"/>
</package>
<package name="CTRIMTZ03" urn="urn:adsk.eagle:footprint:23292/1">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="1.45" y1="-2.5" x2="-1.45" y2="-2.5" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-2.5" x2="-1.05" y2="2.7" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="1.05" y1="2.7" x2="1.45" y2="-2.5" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="-1.05" y1="2.7" x2="1.05" y2="2.7" width="0.254" layer="51" curve="-42.501011"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-2.6" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.6" y="-2.6" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.35" y1="-1.2" x2="0.35" y2="1.2" layer="21"/>
<rectangle x1="-1.2" y1="-0.35" x2="1.2" y2="0.35" layer="21"/>
</package>
<package name="CTRIM808-BC" urn="urn:adsk.eagle:footprint:23276/1">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="-3.4036" y1="1.016" x2="-1.016" y2="3.4036" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="1.016" y1="3.4036" x2="3.4036" y2="1.016" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="-3.4036" y1="-1.016" x2="3.4036" y2="-1.016" width="0.254" layer="21" curve="146.758486"/>
<circle x="0" y="0" radius="3.556" width="0.254" layer="51"/>
<circle x="0" y="0" radius="1.271" width="0.1524" layer="21"/>
<pad name="2" x="0" y="3.6068" drill="1.3"/>
<pad name="1" x="-3.6068" y="0" drill="1.3"/>
<pad name="3" x="3.6068" y="0" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="-0.3048" x2="1.2192" y2="0.3048" layer="21"/>
</package>
<package name="CTRIM808-1" urn="urn:adsk.eagle:footprint:23293/1">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; STELCO GmbH&lt;p&gt;
diameter 8.6 mm, grid 3.55 mm</description>
<wire x1="-3.4925" y1="1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="21" curve="-132.886424"/>
<wire x1="-1.5227" y1="-3.4925" x2="1.5227" y2="-3.4925" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="1.5227" x2="-3.4925" y2="-1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="-1.5227" x2="-1.5227" y2="-3.4925" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="0.889" y1="-4.191" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-3.705" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-3.705" x2="0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-1.2443" y1="-0.254" x2="1.2443" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.2443" y1="0.254" x2="1.2443" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="0.889" x2="-4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="0.889" x2="-3.705" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-0.889" x2="-3.705" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.4925" y1="-1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="1.5227" y1="-3.4925" x2="3.4925" y2="-1.5227" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="4.191" y1="0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="-0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="0.889" x2="4.191" y2="0.889" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.556" y="0" drill="1.3208"/>
<pad name="3" x="3.556" y="0" drill="1.3208"/>
<pad name="2" x="0" y="-3.556" drill="1.3208"/>
<text x="-4.445" y="-3.937" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.223" y="-4.318" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.254" x2="1.27" y2="0.254" layer="21"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="CTRIM808-BC7.5" urn="urn:adsk.eagle:footprint:23294/1">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="1.3" y1="5.05" x2="3.35" y2="-1.6" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-3.35" y1="-1.6" x2="-1.3" y2="5.05" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-1.45" y1="-3" x2="1.45" y2="-3" width="0.254" layer="21" curve="39.851161"/>
<circle x="0" y="1" radius="4.25" width="0.254" layer="51"/>
<circle x="0" y="1" radius="1.271" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.3"/>
<pad name="2" x="0" y="5.08" drill="1.3"/>
<pad name="3" x="2.54" y="-2.54" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="0.6952" x2="1.2192" y2="1.3048" layer="21"/>
<hole x="0" y="1" drill="3"/>
</package>
<package name="CTRIMM-7MM">
<circle x="0" y="0" radius="3.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.25" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-1" x2="1" y2="1" width="0.127" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="5" y2="1.5" width="0.127" layer="21"/>
<pad name="R1" x="0" y="3.81" drill="2.4"/>
<pad name="R2" x="0" y="-3.81" drill="2.4"/>
<pad name="S" x="5.08" y="0" drill="2"/>
<text x="2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="Y14H_RELAY">
<pad name="COM1" x="5.08" y="2.54" drill="1" shape="octagon" rot="R90"/>
<pad name="COM2" x="5.08" y="-2.54" drill="1" shape="octagon" rot="R90"/>
<pad name="L1" x="-2.54" y="2.54" drill="1" shape="octagon" rot="R90"/>
<pad name="L2" x="-2.54" y="-2.54" drill="1" shape="octagon" rot="R90"/>
<pad name="NO" x="-5.08" y="2.54" drill="1" shape="octagon" rot="R90"/>
<pad name="NC" x="-5.08" y="-2.54" drill="1" shape="octagon" rot="R90"/>
<wire x1="-6.35" y1="3.81" x2="6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="6.35" y1="-3.81" x2="-6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<text x="-6.35" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.35" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="GB-15AH">
<pad name="SUPPORT@1" x="-5.08" y="2.54" drill="0.8" shape="long"/>
<pad name="SUPPORT@2" x="-5.08" y="-2.54" drill="0.8" shape="long"/>
<pad name="4" x="0" y="2.54" drill="0.8" shape="long"/>
<pad name="5" x="0" y="0" drill="0.8" shape="long"/>
<pad name="6" x="0" y="-2.54" drill="0.8" shape="long"/>
<wire x1="-5.08" y1="3.81" x2="0" y2="3.81" width="0.127" layer="21"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.127" layer="21"/>
<wire x1="0" y1="-3.81" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.81" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-10.16" y2="1.27" width="0.127" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-11.43" y2="1.27" width="0.127" layer="21"/>
<wire x1="-11.43" y1="1.27" x2="-11.43" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-1.27" x2="-10.16" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-1.27" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-10.16" y1="2.54" x2="-12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="-12.7" y1="2.54" x2="-12.7" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="-10.16" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="1.27" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="CTRIM3008" urn="urn:adsk.eagle:package:23767/1" type="box">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3008"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3018_11" urn="urn:adsk.eagle:package:23765/1" type="box">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3018_11"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3018_12" urn="urn:adsk.eagle:package:23768/1" type="box">
<description>Trimm capacitor SMD STELCO GmbH</description>
<packageinstances>
<packageinstance name="CTRIM3018_12"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.427" urn="urn:adsk.eagle:package:23770/1" type="box">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.427"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.428" urn="urn:adsk.eagle:package:23769/1" type="box">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from both sides, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.428"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.448" urn="urn:adsk.eagle:package:23777/1" type="box">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.448"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.450" urn="urn:adsk.eagle:package:23771/1" type="box">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from both sides, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.450"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3040.452" urn="urn:adsk.eagle:package:23772/1" type="box">
<description>Trimm capacitor STELCO GmbH
 7 S-Triko 160 V DC for PCB mounting 
 Adjustable from one side for automatic adjustment, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3040.452"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.504" urn="urn:adsk.eagle:package:23774/1" type="box">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from one side, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.504"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.505" urn="urn:adsk.eagle:package:23773/1" type="box">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from both sides, vertical to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.505"/>
</packageinstances>
</package3d>
<package3d name="CTRIM3050.506" urn="urn:adsk.eagle:package:23775/1" type="box">
<description>Trimm capacitor STELCO GmbH
 5 S-Triko 160 V DC for PCB mounting,
 Adjustable from one side, parallel to PCB</description>
<packageinstances>
<packageinstance name="CTRIM3050.506"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCTZ2" urn="urn:adsk.eagle:package:23780/1" type="box">
<description>Trimm capacitor AVX</description>
<packageinstances>
<packageinstance name="CTRIMCTZ2"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCTZ3" urn="urn:adsk.eagle:package:23776/1" type="box">
<description>Trimm capacitor AVX</description>
<packageinstances>
<packageinstance name="CTRIMCTZ3"/>
</packageinstances>
</package3d>
<package3d name="CTRIMTZBX4" urn="urn:adsk.eagle:package:23778/1" type="box">
<description>Trimm capacitor muRata</description>
<packageinstances>
<packageinstance name="CTRIMTZBX4"/>
</packageinstances>
</package3d>
<package3d name="CTRIMCV05" urn="urn:adsk.eagle:package:23784/1" type="box">
<description>Trimm capacitor BC-Components</description>
<packageinstances>
<packageinstance name="CTRIMCV05"/>
</packageinstances>
</package3d>
<package3d name="CTRIMTZ03" urn="urn:adsk.eagle:package:23779/1" type="box">
<description>Trimm capacitor muRata</description>
<packageinstances>
<packageinstance name="CTRIMTZ03"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-BC" urn="urn:adsk.eagle:package:23766/1" type="box">
<description>Trimm capacitor  BC-Components</description>
<packageinstances>
<packageinstance name="CTRIM808-BC"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-1" urn="urn:adsk.eagle:package:23781/1" type="box">
<description>Trimm capacitor  STELCO GmbH
diameter 8.6 mm, grid 3.55 mm</description>
<packageinstances>
<packageinstance name="CTRIM808-1"/>
</packageinstances>
</package3d>
<package3d name="CTRIM808-BC7.5" urn="urn:adsk.eagle:package:23783/1" type="box">
<description>Trimm capacitor  BC-Components</description>
<packageinstances>
<packageinstance name="CTRIM808-BC7.5"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CENTERTAP-COIL">
<pin name="4" x="5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="5" x="5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="1" x="-5.08" y="5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="3" x="-5.08" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="180"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="180"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SHELL">
<pin name="GND@1" x="0" y="2.54" visible="pin" length="short" direction="pas"/>
<pin name="GND@2" x="0" y="-2.54" visible="pin" length="short" direction="pas"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VR">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-10.922" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<pin name="BR" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="FILTER">
<pin name="IN" x="-5.08" y="5.08" length="point" rot="R90"/>
<pin name="OUT" x="5.08" y="5.08" length="point" rot="R90"/>
<pin name="COM" x="0" y="-2.54" length="point" rot="R180"/>
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.905" x2="3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-3.175" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="Q">
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="CC">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3764" cap="flat"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
</symbol>
<symbol name="MICROPHON">
<pin name="+" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="-" x="10.16" y="-2.54" length="middle" rot="R180"/>
<circle x="0" y="0" radius="5.588" width="0.254" layer="94"/>
<wire x1="-5.842" y1="5.334" x2="-5.842" y2="-5.334" width="0.508" layer="94"/>
<text x="-5.08" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SWITCH-A">
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<pin name="P" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="5.08" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.175" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="C-TRIMM">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-3.048" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="0" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-2.286" y1="0.762" x2="-1.524" y2="1.524" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="-3.302" x2="-3.048" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-3.302" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-2.794" y2="-1.778" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.286" y="-5.207" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="E" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="Y14H_RELAY">
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-20.32" y1="7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-12.7" y2="5.08" width="0.254" layer="94"/>
<pin name="L1" x="0" y="10.16" length="point" direction="pas" function="dot"/>
<pin name="L2" x="0" y="-10.16" length="point" direction="pas" function="dot"/>
<pin name="COM1" x="7.62" y="10.16" length="point" direction="pas" function="dot"/>
<pin name="COM2" x="7.62" y="-10.16" length="point" direction="pas" function="dot"/>
<pin name="NC" x="-20.32" y="7.62" length="point" direction="pas" function="dot"/>
<pin name="NO" x="-20.32" y="-7.62" length="point" direction="pas" function="dot"/>
<text x="-20.32" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-20.32" y="10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TOGGLESW">
<wire x1="-3.81" y1="1.905" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<pin name="P" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="5.08" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<pin name="O" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="-0.762" y1="0" x2="-0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FCZ-COIL" prefix="L">
<gates>
<gate name="G$1" symbol="CENTERTAP-COIL" x="0" y="0"/>
<gate name="G$2" symbol="SHELL" x="0" y="-12.7" addlevel="request"/>
</gates>
<devices>
<device name="7MM" package="FCZ-7MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="6"/>
<connect gate="G$2" pin="GND@1" pad="7"/>
<connect gate="G$2" pin="GND@2" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="FCZ-10MM">
<connects>
<connect gate="G$1" pin="1" pad="1S"/>
<connect gate="G$1" pin="2" pad="1C"/>
<connect gate="G$1" pin="3" pad="1E"/>
<connect gate="G$1" pin="4" pad="2S"/>
<connect gate="G$1" pin="5" pad="2E"/>
<connect gate="G$2" pin="GND@1" pad="P$6"/>
<connect gate="G$2" pin="GND@2" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VR" prefix="VR" uservalue="yes">
<gates>
<gate name="G$1" symbol="VR" x="0" y="2.54"/>
</gates>
<devices>
<device name="SMD1" package="VR_SM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="BR" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="VR-637A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="BR" pad="BR"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ST-EB" package="ST-EB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="BR" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2" package="VR2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="BR" pad="BR"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CFULA" prefix="XF">
<gates>
<gate name="G$1" symbol="FILTER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CFULA">
<connects>
<connect gate="G$1" pin="COM" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="X" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TINY" package="TINYCRYSTAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR6028" package="NR6028T">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NR8040" package="NR8040T220M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CC" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CC" x="0" y="0"/>
</gates>
<devices>
<device name="-153CLV-0405" package="153CLV-0405">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0505" package="153CLV-0505">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0605" package="153CLV-0605">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0807" package="153CLV-0807">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-0810" package="153CLV-0810">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1010" package="153CLV-1010">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1012" package="153CLV-1012">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-153CLV-1014" package="153CLV-1014">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-5" package="E2,5-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-6" package="E2,5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.5-7" package="E2,5-7">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10.5" package="E5-10,5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-13" package="E5-13">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-5" package="E5-5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-6" package="E5-6">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-8.5" package="E5-8,5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10.5A" package="E5-10,5A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-10,5A2" package="E5-10,5A2">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5-21" package="E5-21">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROPHONE">
<description>Electret Condenser Microphone</description>
<gates>
<gate name="G$1" symbol="MICROPHON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MICROPHONE">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VR-SW" prefix="VR">
<gates>
<gate name="G$1" symbol="SWITCH-A" x="-7.62" y="0"/>
<gate name="G$2" symbol="VR" x="7.62" y="2.54"/>
</gates>
<devices>
<device name="PTR901" package="PTR901">
<connects>
<connect gate="G$1" pin="P" pad="S1"/>
<connect gate="G$1" pin="S" pad="S2"/>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="3"/>
<connect gate="G$2" pin="BR" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-TRIMM" prefix="C" uservalue="yes">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="C-TRIMM" x="0" y="0"/>
</gates>
<devices>
<device name="3008" package="CTRIM3008">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23767/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3018_11" package="CTRIM3018_11">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23765/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3018_12" package="CTRIM3018_12">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23768/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.427" package="CTRIM3040.427">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23770/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.428" package="CTRIM3040.428">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23769/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.448" package="CTRIM3040.448">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23777/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.450" package="CTRIM3040.450">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23771/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3040.452" package="CTRIM3040.452">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23772/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.504" package="CTRIM3050.504">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23774/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.505" package="CTRIM3050.505">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23773/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="3050.506" package="CTRIM3050.506">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23775/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTZ2" package="CTRIMCTZ2">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23780/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTZ3" package="CTRIMCTZ3">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23776/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="TZBX4" package="CTRIMTZBX4">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23778/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="CV05" package="CTRIMCV05">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23784/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="TZ03" package="CTRIMTZ03">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23779/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808" package="CTRIM808-BC">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23766/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808-1" package="CTRIM808-1">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23781/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="808-7.5" package="CTRIM808-BC7.5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23783/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="7MM" package="CTRIMM-7MM">
<connects>
<connect gate="G$1" pin="A" pad="S"/>
<connect gate="G$1" pin="E" pad="R1 R2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="A" pinorder="1"/>
<pinmap gate="G$1" pin="E" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
<deviceset name="Y14H_RELAY" prefix="RELAY">
<gates>
<gate name="G$1" symbol="Y14H_RELAY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="Y14H_RELAY">
<connects>
<connect gate="G$1" pin="COM1" pad="COM1"/>
<connect gate="G$1" pin="COM2" pad="COM2"/>
<connect gate="G$1" pin="L1" pad="L1"/>
<connect gate="G$1" pin="L2" pad="L2"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NO" pad="NO"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PUSHSW-TYPEG" prefix="SW">
<gates>
<gate name="G$1" symbol="TOGGLESW" x="0" y="0"/>
<gate name="G$2" symbol="SHELL" x="-12.7" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="GB-15AH">
<connects>
<connect gate="G$1" pin="O" pad="6"/>
<connect gate="G$1" pin="P" pad="5"/>
<connect gate="G$1" pin="S" pad="4"/>
<connect gate="G$2" pin="GND@1" pad="SUPPORT@1"/>
<connect gate="G$2" pin="GND@2" pad="SUPPORT@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-fet">
<description>&lt;b&gt;Field Effect Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
Symbols changed according to IEC617&lt;p&gt; 
All types, packages and assignment to symbols and pins checked&lt;p&gt;
Package outlines partly checked&lt;p&gt;
&lt;p&gt;
JFET = junction FET&lt;p&gt;
IGBT-x = insulated gate bipolar transistor&lt;p&gt;
x=N: NPN; x=P: PNP&lt;p&gt;
IGFET-mc-nnn; (IGFET=insulated gate field effect transistor)&lt;P&gt;
m=D: depletion mode (Verdr&amp;auml;ngungstyp)&lt;p&gt;
m=E: enhancement mode (Anreicherungstyp)&lt;p&gt;
c: N=N-channel; P=P-Channel&lt;p&gt;
GDSB: gate, drain, source, bulk&lt;p&gt;
&lt;p&gt;
by R. Vogg  15.March.2002</description>
<packages>
<package name="SOT143">
<description>&lt;b&gt;SOT-143&lt;/b&gt;</description>
<wire x1="-1.448" y1="0.635" x2="1.448" y2="0.635" width="0.1" layer="51"/>
<wire x1="-1.448" y1="-0.635" x2="1.448" y2="-0.635" width="0.1" layer="51"/>
<wire x1="-1.448" y1="-0.635" x2="-1.448" y2="0.635" width="0.1" layer="51"/>
<wire x1="1.448" y1="-0.635" x2="1.448" y2="0.635" width="0.1" layer="51"/>
<smd name="4" x="-0.95" y="1.1" dx="1" dy="1.44" layer="1"/>
<smd name="3" x="0.95" y="1.1" dx="1" dy="1.44" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.44" layer="1"/>
<smd name="1" x="-0.75" y="-1.1" dx="1.2" dy="1.44" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.7366" y1="-1.3208" x2="1.1938" y2="-0.635" layer="51"/>
<rectangle x1="0.7112" y1="0.635" x2="1.1684" y2="1.3208" layer="51"/>
<rectangle x1="-1.143" y1="0.635" x2="-0.6858" y2="1.3208" layer="51"/>
<rectangle x1="-1.1938" y1="-1.3208" x2="-0.3048" y2="-0.635" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="DUAL-GATE-N_MOSFET">
<wire x1="-5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="0" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.302" x2="-1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="-1.016" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-5.588" y1="-1.524" x2="-4.572" y2="-1.524" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-1.524" x2="-4.318" y2="-1.27" width="0.1016" layer="94"/>
<wire x1="-5.588" y1="-1.524" x2="-5.842" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-1.524" x2="-2.794" y2="-1.524" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-1.524" x2="-2.54" y2="-1.27" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-1.524" x2="-4.064" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-2.032" x2="-3.81" y2="-2.032" width="0.1016" layer="94"/>
<wire x1="-3.81" y1="-2.032" x2="-4.064" y2="-2.286" width="0.1016" layer="94"/>
<wire x1="-2.794" y1="-2.032" x2="-2.54" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-2.032" x2="-5.588" y2="-2.032" width="0.1016" layer="94"/>
<wire x1="-5.588" y1="-2.032" x2="-5.842" y2="-2.286" width="0.1016" layer="94"/>
<wire x1="-4.572" y1="-2.032" x2="-4.318" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-3.556" x2="-3.302" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-3.556" x2="2.54" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="0" x2="-3.302" y2="-3.556" width="0.1524" layer="94"/>
<circle x="2.54" y="-1.27" radius="0.3592" width="0" layer="94"/>
<circle x="-5.08" y="2.54" radius="0.3592" width="0" layer="94"/>
<circle x="-3.302" y="0" radius="0.3592" width="0" layer="94"/>
<circle x="-3.302" y="-3.556" radius="0.3592" width="0" layer="94"/>
<circle x="2.54" y="-3.556" radius="0.3592" width="0" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.112" y="2.794" size="1.016" layer="93">G1</text>
<text x="-7.112" y="0.254" size="1.016" layer="93">G2</text>
<pin name="G1" x="-7.62" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="G2" x="-7.62" y="0" visible="off" length="middle" direction="pas"/>
<pin name="DRAIN" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="SOURCE" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="-1.27" y="0.762"/>
<vertex x="0" y="1.27"/>
<vertex x="0" y="0.254"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-5.08" y="-1.524"/>
<vertex x="-5.588" y="-0.762"/>
<vertex x="-4.572" y="-0.762"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-3.302" y="-1.524"/>
<vertex x="-3.81" y="-0.762"/>
<vertex x="-2.794" y="-0.762"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-3.302" y="-2.032"/>
<vertex x="-2.794" y="-2.794"/>
<vertex x="-3.81" y="-2.794"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="-5.08" y="-2.032"/>
<vertex x="-4.572" y="-2.794"/>
<vertex x="-5.588" y="-2.794"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="3SK294" prefix="Q">
<gates>
<gate name="G$1" symbol="DUAL-GATE-N_MOSFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT143">
<connects>
<connect gate="G$1" pin="DRAIN" pad="1"/>
<connect gate="G$1" pin="G1" pad="3"/>
<connect gate="G$1" pin="G2" pad="4"/>
<connect gate="G$1" pin="SOURCE" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistor_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Transitor Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC62">
<description>SC-62 (SOT89)</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<smd name="B" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="E" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="C" x="0" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<smd name="C@1" x="0" y="1.575" dx="2.032" dy="1.27" layer="1"/>
<rectangle x1="-0.89" y1="1.19" x2="0.89" y2="2.11" layer="51"/>
<wire x1="-0.7112" y1="-0.4064" x2="-0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="-0.7112" y1="0.9144" x2="0.7112" y2="0.9144" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="0.9144" x2="0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.7112" y1="-0.4064" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="-0.4064" x2="-0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.7112" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="-0.2032" y1="-0.4064" x2="-0.2032" y2="0.508" width="0.6096" layer="1"/>
<wire x1="0.2032" y1="0.508" x2="0.2032" y2="-0.4064" width="0.6096" layer="1"/>
<wire x1="0" y1="-1.4224" x2="0" y2="-0.7112" width="0.6096" layer="1"/>
</package>
<package name="SOT89">
<description>SC-62 (SOT89)</description>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<smd name="1" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="3" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="2" x="0" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<smd name="2@1" x="0" y="1.575" dx="2.032" dy="1.27" layer="1"/>
<rectangle x1="-0.89" y1="1.19" x2="0.89" y2="2.11" layer="51"/>
</package>
<package name="SC70">
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="E" x="0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<smd name="B" x="-0.65" y="-0.95" dx="0.6" dy="0.8" layer="1"/>
<rectangle x1="-0.15" y1="0.625" x2="0.15" y2="1.05" layer="51"/>
<rectangle x1="0.5" y1="-1.05" x2="0.8" y2="-0.625" layer="51"/>
<rectangle x1="-0.8" y1="-1.05" x2="-0.5" y2="-0.625" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
</package>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PNP">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.086" y1="1.678" x2="1.578" y2="2.594" width="0.1524" layer="94"/>
<wire x1="1.578" y1="2.594" x2="0.516" y2="1.478" width="0.1524" layer="94"/>
<wire x1="0.516" y1="1.478" x2="2.086" y2="1.678" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.808" y2="2.124" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.508" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.905" y1="1.778" x2="1.524" y2="2.413" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.413" x2="0.762" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.651" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.778" x2="1.524" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.524" y1="2.159" x2="1.143" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.143" y1="1.905" x2="1.524" y2="1.905" width="0.254" layer="94"/>
</symbol>
<symbol name="NFET">
<pin name="S" x="5.08" y="-2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<text x="-2.54" y="0.635" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="3.175" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="D" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="-2.54" x2="-1.2192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="3.683" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.397" width="0.254" layer="94"/>
<wire x1="1.905" y1="0.635" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.397" x2="0" y2="-3.683" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.54" x2="-1.143" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="NPN">
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2SA1213" prefix="Q">
<gates>
<gate name="G$1" symbol="PNP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC62">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RD01MUS1" prefix="Q">
<gates>
<gate name="G$1" symbol="NFET" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT89">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2 2@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DTC123E" prefix="TR">
<description>ROHM Digital Transistor</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="UA" package="SC70">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA" package="SC59">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="linear">
<description>&lt;b&gt;Linear Devices&lt;/b&gt;&lt;p&gt;
Operational amplifiers,  comparators, voltage regulators, ADCs, DACs, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL08">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="5.08" y1="2.921" x2="-5.08" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="5.08" y1="2.921" x2="5.08" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.921" x2="-5.08" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-2.921" x2="-5.08" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.016" x2="-5.08" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.27" rot="R90"/>
<text x="-5.334" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package 8&lt;/b&gt;&lt;br&gt;
NS Package M08A</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
</package>
<package name="MMSOP08">
<description>&lt;b&gt;Molded Mini Small Outline Package&lt;/b&gt; 8 - Lead (0.118" Wide)&lt;p&gt;
NS Package Number MUA08A&lt;br&gt;
Source: http://cache.national.com/ds/LM/LM386.pdf</description>
<wire x1="1.275" y1="1.425" x2="1.424" y2="1.274" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.424" y1="1.274" x2="1.424" y2="-1.276" width="0.1524" layer="21"/>
<wire x1="1.424" y1="-1.276" x2="1.275" y2="-1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.275" y1="-1.425" x2="-1.275" y2="-1.425" width="0.1524" layer="21"/>
<wire x1="-1.275" y1="-1.425" x2="-1.424" y2="-1.274" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.424" y1="-1.274" x2="-1.424" y2="1.276" width="0.1524" layer="21"/>
<wire x1="-1.424" y1="1.276" x2="-1.275" y2="1.425" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.275" y1="1.425" x2="1.275" y2="1.425" width="0.1524" layer="21"/>
<circle x="-0.9456" y="-0.7906" radius="0.2339" width="0.0508" layer="21"/>
<smd name="8" x="-0.975" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="7" x="-0.325" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="6" x="0.325" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="5" x="0.975" y="2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="4" x="0.975" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="3" x="0.325" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="2" x="-0.325" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<smd name="1" x="-0.975" y="-2.4" dx="0.41" dy="1.02" layer="1"/>
<text x="-2.032" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.302" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.0975" y1="-2.45" x2="-0.8537" y2="-1.85" layer="51"/>
<rectangle x1="-1.0975" y1="-1.85" x2="-0.8537" y2="-1.4256" layer="21"/>
<rectangle x1="-0.4475" y1="-2.45" x2="-0.2037" y2="-1.85" layer="51"/>
<rectangle x1="-0.4475" y1="-1.85" x2="-0.2037" y2="-1.4256" layer="21"/>
<rectangle x1="0.2025" y1="-2.45" x2="0.4463" y2="-1.85" layer="51"/>
<rectangle x1="0.2025" y1="-1.85" x2="0.4463" y2="-1.4256" layer="21"/>
<rectangle x1="0.8525" y1="-2.45" x2="1.0963" y2="-1.85" layer="51"/>
<rectangle x1="0.8525" y1="-1.85" x2="1.0963" y2="-1.4256" layer="21"/>
<rectangle x1="0.8537" y1="1.85" x2="1.0975" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="0.8537" y1="1.4256" x2="1.0975" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="0.2037" y1="1.85" x2="0.4475" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="0.2037" y1="1.4256" x2="0.4475" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="-0.4463" y1="1.85" x2="-0.2025" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-0.4463" y1="1.4256" x2="-0.2025" y2="1.85" layer="21" rot="R180"/>
<rectangle x1="-1.0963" y1="1.85" x2="-0.8525" y2="2.45" layer="51" rot="R180"/>
<rectangle x1="-1.0963" y1="1.4256" x2="-0.8525" y2="1.85" layer="21" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="AMP_GND_GAIN_BYP">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="4.1402" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="2.54" y2="-2.032" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="7.62" y2="0" width="0.4064" layer="94"/>
<wire x1="7.62" y1="0" x2="5.08" y2="1.016" width="0.4064" layer="94"/>
<wire x1="5.08" y1="1.016" x2="2.54" y2="2.032" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.032" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-4.1402" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="2.032" width="0.1524" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="6.096" y="2.286" size="0.6096" layer="93" rot="R90">GAIN</text>
<text x="3.556" y="2.54" size="0.6096" layer="93" rot="R90">GAIN</text>
<text x="-1.27" y="4.826" size="0.6096" layer="93" rot="R90">VS</text>
<text x="-1.27" y="-6.858" size="0.6096" layer="93" rot="R90">GND</text>
<text x="3.556" y="-4.318" size="0.6096" layer="93" rot="R90">BYP</text>
<pin name="GAIN@1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="GAIN@2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="OUT" x="10.16" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="VS" x="-2.54" y="7.62" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="GND" x="-2.54" y="-7.62" visible="pad" length="short" direction="pwr" rot="R90"/>
<pin name="BYPASS" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM386?-*" prefix="IC">
<description>&lt;b&gt;Low Voltage Audio Power Amplifier&lt;/b&gt;&lt;p&gt;
Source: http://cache.national.com/ds/LM/LM386.pdf</description>
<gates>
<gate name="G$1" symbol="AMP_GND_GAIN_BYP" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
<technology name="3"/>
<technology name="4"/>
</technologies>
</device>
<device name="M" package="SO08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
</technologies>
</device>
<device name="MM" package="MMSOP08">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="BYPASS" pad="7"/>
<connect gate="G$1" pin="GAIN@1" pad="1"/>
<connect gate="G$1" pin="GAIN@2" pad="8"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VS" pad="6"/>
</connects>
<technologies>
<technology name="1"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<description>&lt;b&gt;Crystals and Crystal Resonators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="UM1">
<description>&lt;b&gt;Crystal Filter&lt;/b&gt;&lt;p&gt;
Source: www.ilsiamerica.com .. C1 IXF Series.pdf</description>
<wire x1="-2.396" y1="-1.411" x2="2.421" y2="-1.411" width="0.4064" layer="21"/>
<wire x1="-2.421" y1="1.361" x2="2.396" y2="1.361" width="0.4064" layer="21"/>
<wire x1="-2.396" y1="-0.903" x2="2.421" y2="-0.903" width="0.1524" layer="21"/>
<wire x1="2.396" y1="0.853" x2="-2.421" y2="0.853" width="0.1524" layer="21"/>
<wire x1="2.396" y1="0.853" x2="2.421" y2="-0.903" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.396" y1="1.361" x2="2.421" y2="-1.411" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.421" y1="1.361" x2="-2.396" y2="-1.411" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.421" y1="0.853" x2="-2.396" y2="-0.903" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-1.875" y="0" drill="0.7" diameter="1.2"/>
<pad name="2" x="-0.027" y="0" drill="0.7" diameter="1.2"/>
<pad name="3" x="1.875" y="0" drill="0.7" diameter="1.2"/>
<text x="-3.81" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="HC49U">
<description>&lt;b&gt;Crystal Filter&lt;/b&gt;&lt;p&gt;
Source: www.ilsiamerica.com .. C1 IXF Series.pdf</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.1524" layer="51"/>
<wire x1="0.25" y1="1" x2="0.25" y2="-0.25" width="0.1524" layer="51"/>
<wire x1="0.25" y1="-0.25" x2="-0.25" y2="-0.25" width="0.1524" layer="51"/>
<wire x1="-0.25" y1="-0.25" x2="-0.25" y2="1" width="0.1524" layer="51"/>
<wire x1="0.75" y1="1" x2="0.75" y2="0.25" width="0.1524" layer="51"/>
<wire x1="0.75" y1="0.25" x2="0.75" y2="-0.5" width="0.1524" layer="51"/>
<wire x1="-0.75" y1="1" x2="-0.75" y2="0.25" width="0.1524" layer="51"/>
<wire x1="-0.75" y1="0.25" x2="-0.75" y2="-0.5" width="0.1524" layer="51"/>
<wire x1="0.75" y1="0.25" x2="1.5" y2="0.25" width="0.1524" layer="51"/>
<wire x1="-0.75" y1="0.25" x2="-1.5" y2="0.25" width="0.1524" layer="51"/>
<wire x1="-0.75" y1="-0.5" x2="0.75" y2="-0.5" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.25" x2="0" y2="-0.625" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="0" drill="0.7" diameter="1.2"/>
<pad name="2" x="-0.027" y="0" drill="0.7" diameter="1.2"/>
<pad name="3" x="2.413" y="0" drill="0.7" diameter="1.2"/>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-FILTER-3-POL">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.905" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.905" x2="1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.1524" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="-1.016" y="-3.683" size="0.8636" layer="93">2</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">3</text>
<pin name="3" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ECS" prefix="QF">
<description>&lt;b&gt;MONOLITHIC CRYSTAL FILTERS&lt;/b&gt;&lt;p&gt;
Source: http://www.presto.co.uk</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-FILTER-3-POL" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="UM1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="-10.7-15">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-10.7-30">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-21K12">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-21K15">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-21K30">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-21K7.5">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-45K15">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-45K20">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-45K30">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-45K7.5">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-55K15">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-55K20">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-55K30">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-55K32">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-70K15">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-70K20">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-90K20">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="HC49U">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="-10.7-7.5A">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="ECS-10.7-12A">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="ECS-10.7-15A">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="ECS-10.7-30A">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="CTRIM3008">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<pad name="+" x="-1.875" y="0" drill="1"/>
<pad name="-" x="1.875" y="0" drill="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.5" y1="-0.9" x2="-2.25" y2="0.9" layer="51"/>
<rectangle x1="2.25" y1="-1" x2="2.45" y2="1" layer="51"/>
<rectangle x1="2.45" y1="-0.5" x2="2.65" y2="0.5" layer="51"/>
</package>
<package name="CTRIM3018_11">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<smd name="-" x="2.35" y="0" dx="2.3" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-2.6" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="2.6" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3018_12">
<description>&lt;b&gt;Trimm capacitor SMD&lt;/b&gt; STELCO GmbH</description>
<wire x1="-2.15" y1="1.9" x2="2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="1.9" x2="2.15" y2="0.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="0.9" x2="2.15" y2="-0.9" width="0.254" layer="51"/>
<wire x1="2.15" y1="-0.9" x2="2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="2.15" y1="-1.9" x2="-2.15" y2="-1.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-1.9" x2="-2.15" y2="-0.9" width="0.254" layer="21"/>
<wire x1="-2.15" y1="-0.9" x2="-2.15" y2="0.9" width="0.254" layer="51"/>
<wire x1="-2.15" y1="0.9" x2="-2.15" y2="1.9" width="0.254" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1016" layer="21" curve="-120.510237"/>
<wire x1="-1.4" y1="-0.8" x2="1.4" y2="-0.8" width="0.1016" layer="21" curve="120.510237"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1016" layer="51" curve="59.489763"/>
<wire x1="1.4" y1="-0.8" x2="1.4" y2="0.8" width="0.1016" layer="51" curve="59.489763"/>
<smd name="+" x="-3" y="0" dx="2" dy="1.6" layer="1"/>
<smd name="-" x="3" y="0" dx="2" dy="1.6" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.25" x2="0.25" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-0.25" x2="1.25" y2="0.25" layer="21"/>
<rectangle x1="-3.5" y1="-0.6" x2="-2.25" y2="0.6" layer="51"/>
<rectangle x1="2.25" y1="-0.6" x2="3.5" y2="0.6" layer="51"/>
</package>
<package name="CTRIM3040.427">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.35" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.35" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3040.428">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="0.3" y1="1.5" x2="-0.7" y2="-1.3" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-1.5" x2="0.7" y2="1.3" width="0.1524" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
<hole x="0" y="0" drill="4.5"/>
</package>
<package name="CTRIM3040.448">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.450">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from both sides, parallel to PCB</description>
<wire x1="-3.75" y1="2.91" x2="3.75" y2="2.91" width="0.254" layer="51"/>
<wire x1="3.75" y1="2.91" x2="3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.75" y1="-2.54" x2="-3.75" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-3.75" y1="-2.54" x2="-3.75" y2="2.91" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-2.64" x2="-1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-3.64" x2="-0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.64" x2="1.5" y2="-3.64" width="0.254" layer="21"/>
<wire x1="1.5" y1="-3.64" x2="1.5" y2="-2.64" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.64" x2="-0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="-0.45" y1="-3.14" x2="0.45" y2="-3.14" width="0.254" layer="21"/>
<wire x1="0.45" y1="-3.14" x2="0.45" y2="-3.64" width="0.254" layer="21"/>
<wire x1="-1.55" y1="2.91" x2="1.5" y2="2.91" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="3.81" drill="1.3"/>
<pad name="1B" x="2.5" y="3.81" drill="1.3"/>
<pad name="2" x="0" y="1.31" drill="1.3"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="3.01" x2="-2.2" y2="4.56" layer="51"/>
<rectangle x1="2.2" y1="3.01" x2="2.8" y2="4.56" layer="51"/>
</package>
<package name="CTRIM3040.452">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 7 S-Triko 160 V DC for PCB mounting &lt;p&gt;
 Adjustable from one side for automatic adjustment, vertical to PCB</description>
<wire x1="-3.3" y1="1.2" x2="3.3" y2="1.2" width="0.254" layer="21" curve="-140.033787"/>
<wire x1="-3.3" y1="-1.2" x2="3.3" y2="-1.2" width="0.254" layer="21" curve="140.033787"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="-1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="1.2" width="0.254" layer="51" curve="39.966213"/>
<wire x1="-0.85" y1="1.5" x2="-1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="-1.7" y1="0" x2="-0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="-1.5" x2="0.85" y2="-1.5" width="0.1016" layer="21"/>
<wire x1="0.85" y1="-1.5" x2="1.7" y2="0" width="0.1016" layer="21"/>
<wire x1="1.7" y1="0" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-0.85" y1="1.5" x2="0.85" y2="1.5" width="0.1016" layer="21"/>
<wire x1="-1.35" y1="0.45" x2="1.05" y2="-1" width="0.1016" layer="21"/>
<wire x1="-1.05" y1="0.95" x2="1.35" y2="-0.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1016" layer="21"/>
<circle x="0" y="0" radius="1.4508" width="0.1016" layer="21"/>
<pad name="1A" x="-3.5" y="0" drill="1.3"/>
<pad name="2" x="0" y="-3.5" drill="1.3"/>
<pad name="1B" x="3.5" y="0" drill="1.3"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.9" y1="-0.6" x2="-3.5" y2="0.6" layer="51"/>
<rectangle x1="3.5" y1="-0.6" x2="3.9" y2="0.6" layer="51"/>
<rectangle x1="-0.6" y1="-3.9" x2="0.6" y2="-3.5" layer="51"/>
</package>
<package name="CTRIM3050.504">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIM3050.505">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from both sides, vertical to PCB</description>
<wire x1="-0.7" y1="0.4" x2="0.6" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="0.9" x2="1.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-2.4" y1="1" x2="2.8" y2="1" width="0.254" layer="21" curve="-137.924978"/>
<wire x1="-2.4" y1="-1" x2="2.8" y2="-1" width="0.254" layer="21" curve="137.924978"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.254" layer="51" curve="42.075022"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.254" layer="51" curve="42.075022"/>
<circle x="0.2" y="0" radius="1" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="1.1"/>
<pad name="2" x="2.5" y="0" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0.2" y="0" drill="2.5"/>
</package>
<package name="CTRIM3050.506">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; STELCO GmbH&lt;p&gt;
 5 S-Triko 160 V DC for PCB mounting,&lt;p&gt;
 Adjustable from one side, parallel to PCB</description>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-2.63" x2="-1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="1.2" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="2.4" y1="-1.63" x2="2.4" y2="0.22" width="0.254" layer="21"/>
<wire x1="2.4" y1="1.67" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.2" y1="-2.63" x2="-0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.63" x2="-0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="-0.4" y1="-2.13" x2="0.4" y2="-2.13" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.13" x2="0.4" y2="-2.63" width="0.254" layer="21"/>
<wire x1="0.4" y1="-2.63" x2="1.2" y2="-2.63" width="0.254" layer="21"/>
<wire x1="-1.2" y1="-1.63" x2="-2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="1.2" y1="-1.63" x2="2.4" y2="-1.63" width="0.254" layer="21"/>
<wire x1="-2.4" y1="0.22" x2="-2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="2.4" y1="0.22" x2="2.4" y2="1.67" width="0.254" layer="51"/>
<wire x1="-1.45" y1="1.67" x2="1.45" y2="1.67" width="0.254" layer="21"/>
<pad name="1A" x="-2.5" y="1.27" drill="1.1"/>
<pad name="1B" x="2.5" y="1.27" drill="1.1"/>
<pad name="2" x="0" y="-0.23" drill="1.1"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CTRIMCTZ2">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="51"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="-0.45" y1="1.35" x2="0.45" y2="1.35" width="0.254" layer="51"/>
<wire x1="0.45" y1="1.35" x2="1.15" y2="0.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="0.4" x2="1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="1.15" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="51"/>
<wire x1="-0.5" y1="-1.4" x2="-1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="-1.15" y1="-1.4" x2="-1.15" y2="0.45" width="0.254" layer="21"/>
<wire x1="-1.15" y1="0.45" x2="-0.45" y2="1.35" width="0.254" layer="21"/>
<wire x1="0.5" y1="-1.4" x2="1.15" y2="-1.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="-1.4" x2="1.15" y2="0.4" width="0.254" layer="21"/>
<wire x1="1.15" y1="0.4" x2="0.45" y2="1.35" width="0.254" layer="21"/>
<circle x="0" y="0" radius="0.75" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.25" dx="0.5" dy="0.45" layer="1"/>
<smd name="2" x="0" y="-1.25" dx="0.55" dy="0.5" layer="1"/>
<text x="-1.4" y="-1.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.7" y="-1.5" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.55" x2="0.15" y2="0.55" layer="21"/>
<rectangle x1="-0.55" y1="-0.15" x2="0.55" y2="0.15" layer="21"/>
</package>
<package name="CTRIMCTZ3">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; AVX</description>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="-0.45" y1="2.1" x2="0.45" y2="2.1" width="0.254" layer="51"/>
<wire x1="0.45" y1="2.1" x2="1.45" y2="0.75" width="0.254" layer="51"/>
<wire x1="1.45" y1="0.75" x2="1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.45" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-0.6" y1="-2.15" x2="-1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.45" y1="-2.15" x2="-1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="-1.45" y1="0.75" x2="-0.45" y2="2.1" width="0.254" layer="21"/>
<wire x1="0.6" y1="-2.15" x2="1.45" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.45" y1="-2.15" x2="1.45" y2="0.75" width="0.254" layer="21"/>
<wire x1="1.45" y1="0.75" x2="0.45" y2="2.1" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.1" width="0.1524" layer="21"/>
<smd name="1" x="0" y="1.95" dx="0.6" dy="0.6" layer="1"/>
<smd name="2" x="0" y="-1.95" dx="0.78" dy="0.6" layer="1"/>
<text x="-1.85" y="-2.3" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.1" y="-2.3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-0.85" x2="0.25" y2="0.85" layer="21"/>
<rectangle x1="-0.85" y1="-0.25" x2="0.85" y2="0.25" layer="21"/>
</package>
<package name="CTRIMTZBX4">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="-1.9" y1="2.15" x2="-1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="-1.9" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="1.9" y1="2.15" x2="-1.9" y2="2.15" width="0.254" layer="51"/>
<wire x1="-1.05" y1="-2.15" x2="-1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="-2.15" x2="-1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="-1.9" y1="2.15" x2="-1.05" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.05" y1="-2.15" x2="1.9" y2="-2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="-2.15" x2="1.9" y2="2.15" width="0.254" layer="21"/>
<wire x1="1.9" y1="2.15" x2="1.05" y2="2.15" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.5" width="0.1524" layer="51"/>
<smd name="1" x="0" y="1.8" dx="1.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.8" dx="1.6" dy="1.2" layer="1"/>
<text x="-2.3" y="-2.25" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.55" y="-2.25" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.25" y1="-1.2" x2="0.25" y2="1.2" layer="51"/>
<rectangle x1="-1.2" y1="-0.25" x2="1.2" y2="0.25" layer="51"/>
</package>
<package name="CTRIMCV05">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; BC-Components</description>
<wire x1="-2.8" y1="0" x2="2.8" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="-2.8" y1="0" x2="-1.05" y2="2.6" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="1.05" y1="2.6" x2="2.8" y2="0" width="0.254" layer="21" curve="-68.064256"/>
<wire x1="-2.8" y1="0" x2="-2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-2.8" y1="-2.9" x2="-1.1" y2="-2.9" width="0.254" layer="21"/>
<wire x1="-1.1" y1="-2.9" x2="1.15" y2="-2.9" width="0.254" layer="51"/>
<wire x1="1.15" y1="-2.9" x2="2.8" y2="-2.9" width="0.254" layer="21"/>
<wire x1="2.8" y1="-2.9" x2="2.8" y2="0" width="0.254" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="1.15" x2="0" y2="2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="2.25" x2="1.95" y2="1.15" width="0.1524" layer="51"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="51"/>
<wire x1="1.95" y1="-1.1" x2="0" y2="-2.25" width="0.1524" layer="51"/>
<wire x1="0" y1="-2.25" x2="-1.95" y2="-1.15" width="0.1524" layer="51"/>
<wire x1="-1.95" y1="-1.15" x2="-1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="1.15" x2="-0.7" y2="1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="1.85" x2="1.95" y2="1.15" width="0.1524" layer="21"/>
<wire x1="1.95" y1="1.15" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<wire x1="-1.95" y1="-1.15" x2="-0.7" y2="-1.85" width="0.1524" layer="21"/>
<wire x1="0.7" y1="-1.85" x2="1.95" y2="-1.1" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-3.05" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.5" y="-3.05" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.9" y1="-0.5" x2="1.95" y2="0.5" layer="21"/>
</package>
<package name="CTRIMTZ03">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt; muRata</description>
<wire x1="1.45" y1="-2.5" x2="-1.45" y2="-2.5" width="0.254" layer="51"/>
<wire x1="-1.45" y1="-2.5" x2="-1.05" y2="2.7" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="1.05" y1="2.7" x2="1.45" y2="-2.5" width="0.254" layer="21" curve="-128.646369"/>
<wire x1="-1.05" y1="2.7" x2="1.05" y2="2.7" width="0.254" layer="51" curve="-42.501011"/>
<circle x="0" y="0" radius="1.6" width="0.1524" layer="21"/>
<pad name="1" x="0" y="2.5" drill="1"/>
<pad name="2" x="0" y="-2.5" drill="1"/>
<text x="-3.3" y="-2.6" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.6" y="-2.6" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.35" y1="-1.2" x2="0.35" y2="1.2" layer="21"/>
<rectangle x1="-1.2" y1="-0.35" x2="1.2" y2="0.35" layer="21"/>
</package>
<package name="CTRIM808-BC">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="-3.4036" y1="1.016" x2="-1.016" y2="3.4036" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="1.016" y1="3.4036" x2="3.4036" y2="1.016" width="0.254" layer="21" curve="-56.758486"/>
<wire x1="-3.4036" y1="-1.016" x2="3.4036" y2="-1.016" width="0.254" layer="21" curve="146.758486"/>
<circle x="0" y="0" radius="3.556" width="0.254" layer="51"/>
<circle x="0" y="0" radius="1.271" width="0.1524" layer="21"/>
<pad name="2" x="0" y="3.6068" drill="1.3"/>
<pad name="1" x="-3.6068" y="0" drill="1.3"/>
<pad name="3" x="3.6068" y="0" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="-0.3048" x2="1.2192" y2="0.3048" layer="21"/>
</package>
<package name="CTRIM808-1">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; STELCO GmbH&lt;p&gt;
diameter 8.6 mm, grid 3.55 mm</description>
<wire x1="-3.4925" y1="1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="21" curve="-132.886424"/>
<wire x1="-1.5227" y1="-3.4925" x2="1.5227" y2="-3.4925" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="1.5227" x2="-3.4925" y2="-1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="-3.4925" y1="-1.5227" x2="-1.5227" y2="-3.4925" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="0.889" y1="-4.191" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-3.705" x2="-0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-3.705" x2="0.889" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-1.2443" y1="-0.254" x2="1.2443" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.2443" y1="0.254" x2="1.2443" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="0.889" x2="-4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="0.889" x2="-3.705" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-0.889" x2="-3.705" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.4925" y1="-1.5227" x2="3.4925" y2="1.5227" width="0.1524" layer="51" curve="47.113576"/>
<wire x1="1.5227" y1="-3.4925" x2="3.4925" y2="-1.5227" width="0.1524" layer="21" curve="42.886424"/>
<wire x1="4.191" y1="0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="-0.889" x2="4.191" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.705" y1="0.889" x2="4.191" y2="0.889" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.556" y="0" drill="1.3208"/>
<pad name="3" x="3.556" y="0" drill="1.3208"/>
<pad name="2" x="0" y="-3.556" drill="1.3208"/>
<text x="-4.445" y="-3.937" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.223" y="-4.318" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.254" x2="1.27" y2="0.254" layer="21"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="CTRIM808-BC7.5">
<description>&lt;b&gt;Trimm capacitor &lt;/b&gt; BC-Components</description>
<wire x1="1.3" y1="5.05" x2="3.35" y2="-1.6" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-3.35" y1="-1.6" x2="-1.3" y2="5.05" width="0.254" layer="21" curve="-110.073805"/>
<wire x1="-1.45" y1="-3" x2="1.45" y2="-3" width="0.254" layer="21" curve="39.851161"/>
<circle x="0" y="1" radius="4.25" width="0.254" layer="51"/>
<circle x="0" y="1" radius="1.271" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.3"/>
<pad name="2" x="0" y="5.08" drill="1.3"/>
<pad name="3" x="2.54" y="-2.54" drill="1.3"/>
<text x="-4.4684" y="-3.4056" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.77" y="-3.4056" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2192" y1="0.6952" x2="1.2192" y2="1.3048" layer="21"/>
<hole x="0" y="1" drill="3"/>
</package>
<package name="CTRIMCCW12-3">
<pad name="P$1" x="-3" y="0" drill="1" shape="long"/>
<pad name="P$2" x="3" y="0" drill="1" shape="long"/>
<circle x="-1.27" y="0" radius="2.6" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<circle x="-1.27" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.54" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C-TRIMM">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-3.048" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="0" x2="-2.286" y2="0.762" width="0.3048" layer="94"/>
<wire x1="-2.286" y1="0.762" x2="-1.524" y2="1.524" width="0.3048" layer="94"/>
<wire x1="-3.048" y1="-3.302" x2="-3.048" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-3.302" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.016" x2="-2.794" y2="-1.778" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.286" y="-5.207" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="E" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-TRIMM" prefix="C" uservalue="yes">
<description>&lt;b&gt;Trimm capacitor&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="C-TRIMM" x="0" y="0"/>
</gates>
<devices>
<device name="3008" package="CTRIM3008">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3018_11" package="CTRIM3018_11">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3018_12" package="CTRIM3018_12">
<connects>
<connect gate="G$1" pin="A" pad="-"/>
<connect gate="G$1" pin="E" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3040.427" package="CTRIM3040.427">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3040.428" package="CTRIM3040.428">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3040.448" package="CTRIM3040.448">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3040.450" package="CTRIM3040.450">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3040.452" package="CTRIM3040.452">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3050.504" package="CTRIM3050.504">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3050.505" package="CTRIM3050.505">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3050.506" package="CTRIM3050.506">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1A"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CTZ2" package="CTRIMCTZ2">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CTZ3" package="CTRIMCTZ3">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TZBX4" package="CTRIMTZBX4">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CV05" package="CTRIMCV05">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TZ03" package="CTRIMTZ03">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="808" package="CTRIM808-BC">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="808-1" package="CTRIM808-1">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="808-7.5" package="CTRIM808-BC7.5">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CCW12-3" package="CTRIMCCW12-3">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="E" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="MJ-3502">
<pad name="1" x="1.2" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="2" x="6.6" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="3" x="12.3" y="0" drill="1.2" shape="long" rot="R90"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="1.2" layer="46"/>
<wire x1="6.6" y1="-0.65" x2="6.6" y2="0.65" width="1.2" layer="46"/>
<wire x1="12.3" y1="-0.65" x2="12.3" y2="0.65" width="1.2" layer="46"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.2" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="3.75" width="0.2" layer="21"/>
<wire x1="0" y1="3.75" x2="13.5" y2="3.75" width="0.2" layer="21"/>
<wire x1="13.5" y1="3.75" x2="13.5" y2="-3.75" width="0.2" layer="21"/>
<wire x1="13.5" y1="-3.75" x2="0" y2="-3.75" width="0.2" layer="21"/>
<wire x1="0" y1="-3.75" x2="0" y2="-2.54" width="0.2" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.2" layer="21"/>
<wire x1="0" y1="2.54" x2="-5" y2="2.54" width="0.2" layer="21"/>
<wire x1="-5" y1="2.54" x2="-5" y2="-2.54" width="0.2" layer="21"/>
<wire x1="-5" y1="-2.54" x2="0" y2="-2.54" width="0.2" layer="21"/>
</package>
<package name="SJ1-3535NG">
<pad name="1" x="1.2" y="-0.9" drill="1" shape="long" rot="R90"/>
<pad name="5" x="7.3" y="-0.9" drill="1" shape="long" rot="R90"/>
<pad name="4" x="12.8" y="-0.9" drill="1" shape="long" rot="R90"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.2" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="3.75" width="0.2" layer="21"/>
<wire x1="0" y1="3.75" x2="13.5" y2="3.75" width="0.2" layer="21"/>
<wire x1="13.5" y1="3.75" x2="13.5" y2="-3.75" width="0.2" layer="21"/>
<wire x1="13.5" y1="-3.75" x2="0" y2="-3.75" width="0.2" layer="21"/>
<wire x1="0" y1="-3.75" x2="0" y2="-2.54" width="0.2" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.2" layer="21"/>
<wire x1="0" y1="2.54" x2="-5" y2="2.54" width="0.2" layer="21"/>
<wire x1="-5" y1="2.54" x2="-5" y2="-2.54" width="0.2" layer="21"/>
<wire x1="-5" y1="-2.54" x2="0" y2="-2.54" width="0.2" layer="21"/>
<pad name="2" x="3.6" y="1.1" drill="1" shape="long" rot="R90"/>
<pad name="3" x="9.1" y="1.1" drill="1" shape="long" rot="R90"/>
<wire x1="1.2" y1="-0.4" x2="1.2" y2="-1.4" width="1" layer="46"/>
<wire x1="3.6" y1="1.6" x2="3.6" y2="0.6" width="1" layer="46"/>
<wire x1="7.3" y1="-0.4" x2="7.3" y2="-1.4" width="1" layer="46"/>
<wire x1="9.1" y1="0.6" x2="9.1" y2="1.6" width="1" layer="46"/>
<wire x1="12.8" y1="-0.4" x2="12.8" y2="-1.4" width="1" layer="46"/>
</package>
<package name="5251-02-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="2.1321" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<text x="-2.8941" y="-2.8941" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="-1.27" y1="9.906" x2="-1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.016" y1="10.414" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="48"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
<text x="2.286" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-1.016" y="-2.794" size="1.27" layer="51" rot="R90">2</text>
</package>
<package name="5251-02-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-2.5131" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.032" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-0.762" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">2</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<text x="2.5131" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.905" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.397" x2="1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.397" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="IL-G-7P-S3T2-SA">
<pad name="4" x="0" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-9" y1="3.07" x2="9" y2="3.07" width="0.127" layer="21"/>
<wire x1="9" y1="3.07" x2="9" y2="2.54" width="0.127" layer="21"/>
<wire x1="9" y1="2.54" x2="9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="9" y1="-2.93" x2="-9" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-9" y1="-2.93" x2="-9" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="-9" y2="3.07" width="0.127" layer="21"/>
<wire x1="-9" y1="2.54" x2="9" y2="2.54" width="0.127" layer="21"/>
<text x="-8.89" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.89" y="3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-10.16" y="-2.54" size="1.27" layer="21">1</text>
</package>
<package name="IL-G-2P-S3T2-SA">
<pad name="1" x="-1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.1" shape="long" rot="R90"/>
<wire x1="-2.25" y1="3.07" x2="2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="2.25" y1="3.07" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.25" y1="2.54" x2="2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="2.25" y1="-2.93" x2="-2.25" y2="-2.93" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.93" x2="-2.25" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="-2.25" y2="3.07" width="0.127" layer="21"/>
<wire x1="-2.25" y1="2.54" x2="2.25" y2="2.54" width="0.127" layer="21"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="-2.54" size="1.27" layer="21">1</text>
</package>
</packages>
<symbols>
<symbol name="JACK-SWITCH">
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.286" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.286" x2="-1.016" y2="1.524" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.762" y2="2.286" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.096" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.508" x2="7.874" y2="0.508" layer="94" rot="R90"/>
<pin name="SLEEVE" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="SW" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="JACK-2SWITCH">
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.302" y2="3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="-2.032" y2="4.826" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="4.826" x2="-2.286" y2="4.064" width="0.254" layer="94"/>
<wire x1="-2.286" y1="4.064" x2="-1.778" y2="4.064" width="0.254" layer="94"/>
<wire x1="-1.778" y1="4.064" x2="-2.032" y2="4.826" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.524" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="-2.032" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.286" x2="-2.286" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-1.524" x2="-1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.524" x2="-2.032" y2="-2.286" width="0.254" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="2.286" y1="0.762" x2="7.874" y2="1.778" layer="94" rot="R90"/>
<pin name="SLEEVE" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="RING" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="RINGSW" x="-5.08" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="TIPSW" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MONO-JACK" prefix="J">
<gates>
<gate name="G$1" symbol="JACK-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="MJ-3502" package="MJ-3502">
<connects>
<connect gate="G$1" pin="SLEEVE" pad="1"/>
<connect gate="G$1" pin="SW" pad="3"/>
<connect gate="G$1" pin="TIP" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STEREO-JACK" prefix="J">
<gates>
<gate name="G$1" symbol="JACK-2SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="SJ1-3535NG" package="SJ1-3535NG">
<connects>
<connect gate="G$1" pin="RING" pad="3"/>
<connect gate="G$1" pin="RINGSW" pad="5"/>
<connect gate="G$1" pin="SLEEVE" pad="1"/>
<connect gate="G$1" pin="TIP" pad="2"/>
<connect gate="G$1" pin="TIPSW" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RT-2PIN" prefix="CON">
<gates>
<gate name="_SIGNAL" symbol="M-1" x="5.08" y="2.54"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IL-G-S" package="IL-G-2P-S3T2-SA">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RADIO-INTERCONNECT" prefix="CON">
<gates>
<gate name="PTT" symbol="M" x="2.54" y="7.62"/>
<gate name="VAGC" symbol="M" x="2.54" y="5.08"/>
<gate name="+12V" symbol="M" x="2.54" y="2.54"/>
<gate name="PWRSW" symbol="M" x="2.54" y="0"/>
<gate name="TXEN" symbol="M" x="2.54" y="-2.54"/>
<gate name="RXEN" symbol="M" x="2.54" y="-5.08"/>
<gate name="GND" symbol="M" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="IL-G-7P-S3T2-SA">
<connects>
<connect gate="+12V" pin="S" pad="2"/>
<connect gate="GND" pin="S" pad="1"/>
<connect gate="PTT" pin="S" pad="3"/>
<connect gate="PWRSW" pin="S" pad="4"/>
<connect gate="RXEN" pin="S" pad="5"/>
<connect gate="TXEN" pin="S" pad="6"/>
<connect gate="VAGC" pin="S" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1SS196" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="E"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1SS193" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-coax">
<description>&lt;b&gt;Coax Connectors&lt;/b&gt;&lt;p&gt;
Radiall  and M/A COM.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="AMP_227161">
<description>&lt;b&gt;JACK,  RIGHT ANGLE, 50 OHM, PCB, BNC&lt;/b&gt;&lt;p&gt;
Source: amp_227161.pdf</description>
<wire x1="-7.275" y1="-6.875" x2="7.275" y2="-6.875" width="0.2032" layer="21"/>
<wire x1="7.275" y1="-6.875" x2="7.275" y2="7.275" width="0.2032" layer="21"/>
<wire x1="7.275" y1="7.275" x2="-7.275" y2="7.275" width="0.2032" layer="21"/>
<wire x1="-7.275" y1="7.275" x2="-7.275" y2="-6.875" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="7.375" x2="-6.4" y2="16.025" width="0.2032" layer="21"/>
<wire x1="-6.4" y1="16.025" x2="6.4" y2="16.025" width="0.2032" layer="21"/>
<wire x1="6.4" y1="16.025" x2="6.4" y2="7.35" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="16.15" x2="-4.9" y2="28.475" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="28.475" x2="4.9" y2="28.475" width="0.2032" layer="21"/>
<wire x1="4.9" y1="28.475" x2="4.9" y2="16.125" width="0.2032" layer="21"/>
<circle x="0" y="23.94" radius="1.26" width="0" layer="21"/>
<pad name="1" x="0" y="-5.08" drill="0.9" diameter="1.27"/>
<pad name="2" x="-2.54" y="-5.08" drill="0.9" diameter="1.27"/>
<text x="-6.985" y="-8.89" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-8.89" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-5.08" y="0" drill="2"/>
<hole x="5.08" y="0" drill="2"/>
</package>
</packages>
<symbols>
<symbol name="BU-BNC">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="0" y1="-2.54" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0.254" x2="-0.762" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.254" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.762" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-0.254" x2="-2.54" y2="-0.254" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="2.54" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="?227161*" prefix="X">
<description>&lt;b&gt;JACK,  RIGHT ANGLE, 50 OHM, PCB, BNC&lt;/b&gt;&lt;p&gt;
Source: amp_227161.pdf</description>
<gates>
<gate name="G$1" symbol="BU-BNC" x="0" y="0"/>
</gates>
<devices>
<device name="1-" package="AMP_227161">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-0">
<attribute name="MF" value="TYCO ELECTRONICS" constant="no"/>
<attribute name="MPN" value="1-227161" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="33B3200" constant="no"/>
</technology>
<technology name="-2">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="-6">
<attribute name="MF" value="TYCO ELECTRONICS" constant="no"/>
<attribute name="MPN" value="1-227161-6" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="09H7854" constant="no"/>
</technology>
<technology name="-7">
<attribute name="MF" value="TYCO ELECTRONICS" constant="no"/>
<attribute name="MPN" value="1-227161-7" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="09H7855" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="AMP_227161">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="-8">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="IC_Robotech" deviceset="NJM2594" device="V"/>
<part name="IC2" library="IC_Robotech" deviceset="NJM2783" device="V"/>
<part name="Q1" library="transistor-power" deviceset="RD06HVF1" device="H"/>
<part name="L1" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="L2" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="L3" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="C1" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C2" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C3" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C4" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C5" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="Q2" library="transistor-fet" deviceset="3SK294" device=""/>
<part name="Q3" library="Transistor_Robotech" deviceset="2SA1213" device=""/>
<part name="Q4" library="Transistor_Robotech" deviceset="2SA1213" device=""/>
<part name="IC3" library="linear" deviceset="LM386?-*" device="M" technology="1"/>
<part name="VR1" library="Passive_Robotech" deviceset="VR" device="2"/>
<part name="VR2" library="Passive_Robotech" deviceset="VR" device="2"/>
<part name="VR3" library="Passive_Robotech" deviceset="VR" device="2"/>
<part name="Q5" library="Transistor_Robotech" deviceset="RD01MUS1" device=""/>
<part name="L4" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="L5" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="Q6" library="transistor-fet" deviceset="3SK294" device=""/>
<part name="L6" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="Q7" library="transistor-fet" deviceset="3SK294" device=""/>
<part name="Q8" library="transistor-fet" deviceset="3SK294" device=""/>
<part name="IC4" library="IC_Robotech" deviceset="LA1600" device=""/>
<part name="QF1" library="crystal" deviceset="ECS" device="" technology="-10.7-7.5A"/>
<part name="XF1" library="Passive_Robotech" deviceset="CFULA" device=""/>
<part name="L7" library="Passive_Robotech" deviceset="FCZ-COIL" device="7MM"/>
<part name="C6" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C7" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C8" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R14" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R15" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R16" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="X1" library="Passive_Robotech" deviceset="CRYSTAL" device="HC49S"/>
<part name="C18" library="rcl" deviceset="C-TRIMM" device="CCW12-3"/>
<part name="R17" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C19" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C20" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="L8" library="Passive_Robotech" deviceset="L" device="/2.5"/>
<part name="L9" library="Passive_Robotech" deviceset="L" device="/2.5"/>
<part name="R18" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C21" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="C22" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$1" library="IC_Robotech" deviceset="LMC662" device=""/>
<part name="R19" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R20" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R21" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R22" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R23" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C27" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="C28" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="C29" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C30" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="C31" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C32" library="Passive_Robotech" deviceset="CC" device="-2.5-6"/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY27" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="J1" library="Connector_Robotech" deviceset="MONO-JACK" device="MJ-3502"/>
<part name="U$2" library="Passive_Robotech" deviceset="MICROPHONE" device=""/>
<part name="J2" library="Connector_Robotech" deviceset="STEREO-JACK" device="SJ1-3535NG"/>
<part name="VR4" library="Passive_Robotech" deviceset="VR-SW" device="PTR901"/>
<part name="SUPPLY28" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY29" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON1" library="Connector_Robotech" deviceset="RT-2PIN" device="-S"/>
<part name="C33" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R24" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R25" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C34" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R26" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C35" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R27" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C36" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D1" library="Diode_Robotech" deviceset="1SS196" device=""/>
<part name="D2" library="Diode_Robotech" deviceset="1SS196" device=""/>
<part name="C37" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R28" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="L10" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="C38" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R29" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C39" library="rcl" deviceset="C-TRIMM" device="CCW12-3"/>
<part name="C40" library="rcl" deviceset="C-TRIMM" device="CCW12-3"/>
<part name="R30" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C41" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="L11" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="L12" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="C42" library="Passive_Robotech" deviceset="C-TRIMM" device="7MM"/>
<part name="C43" library="Passive_Robotech" deviceset="C-TRIMM" device="7MM"/>
<part name="C44" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C45" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="L13" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="C46" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="L14" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="L15" library="Passive_Robotech" deviceset="L" device="T20V"/>
<part name="C47" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C48" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY40" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R31" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY41" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY42" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C49" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="SUPPLY43" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C50" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C51" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R32" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY44" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R33" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY45" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY46" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C52" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C53" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R34" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R35" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R36" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C54" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C55" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY47" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY48" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R37" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C56" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R38" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C57" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C58" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="R39" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C59" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="SUPPLY49" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C60" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C61" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R40" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R41" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R42" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="SUPPLY51" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY52" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C62" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY53" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="RELAY1" library="Passive_Robotech" deviceset="Y14H_RELAY" device=""/>
<part name="C63" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C64" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY55" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D3" library="Diode_Robotech" deviceset="1SS193" device=""/>
<part name="SUPPLY56" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY57" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="X2" library="con-coax" deviceset="?227161*" device="1-" technology="-0"/>
<part name="SUPPLY58" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="TR1" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="TR2" library="Transistor_Robotech" deviceset="DTC123E" device="KA"/>
<part name="R43" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R44" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R45" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R46" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="IC5" library="IC_Robotech" deviceset="78*" device=""/>
<part name="C65" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C66" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY59" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY60" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY61" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="IC6" library="IC_Robotech" deviceset="TA78L*F" device="" technology="06"/>
<part name="C67" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY62" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY63" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY64" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY65" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY66" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SW1" library="Passive_Robotech" deviceset="PUSHSW-TYPEG" device=""/>
<part name="SUPPLY68" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON2" library="Connector_Robotech" deviceset="RADIO-INTERCONNECT" device=""/>
<part name="CON3" library="Connector_Robotech" deviceset="RT-2PIN" device="IL-G-S"/>
<part name="CON4" library="Connector_Robotech" deviceset="RT-2PIN" device="IL-G-S"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="12.7" y="81.28" rot="MR0"/>
<instance part="IC2" gate="A" x="-45.72" y="91.44" rot="R90"/>
<instance part="Q1" gate="G$1" x="198.12" y="88.9"/>
<instance part="L1" gate="G$1" x="48.26" y="76.2" rot="MR0"/>
<instance part="L2" gate="G$1" x="81.28" y="76.2"/>
<instance part="L3" gate="G$1" x="134.62" y="91.44"/>
<instance part="C1" gate="G$1" x="58.42" y="76.2"/>
<instance part="C2" gate="G$1" x="35.56" y="104.14"/>
<instance part="C3" gate="G$1" x="-10.16" y="76.2"/>
<instance part="C4" gate="G$1" x="63.5" y="81.28" rot="R90"/>
<instance part="C5" gate="G$1" x="71.12" y="76.2"/>
<instance part="Q2" gate="G$1" x="109.22" y="86.36"/>
<instance part="Q3" gate="G$1" x="198.12" y="55.88" rot="MR90"/>
<instance part="Q4" gate="G$1" x="193.04" y="38.1" rot="MR90"/>
<instance part="IC3" gate="G$1" x="157.48" y="22.86"/>
<instance part="VR1" gate="G$1" x="187.96" y="63.5" rot="R180"/>
<instance part="VR2" gate="G$1" x="-60.96" y="48.26" rot="MR270"/>
<instance part="VR3" gate="G$1" x="15.24" y="55.88" rot="R270"/>
<instance part="Q5" gate="G$1" x="157.48" y="96.52"/>
<instance part="L4" gate="G$1" x="15.24" y="30.48" rot="MR180"/>
<instance part="L5" gate="G$1" x="33.02" y="30.48" rot="R180"/>
<instance part="Q6" gate="G$1" x="-2.54" y="30.48"/>
<instance part="L6" gate="G$1" x="-33.02" y="25.4" rot="MR0"/>
<instance part="Q7" gate="G$1" x="63.5" y="35.56"/>
<instance part="Q8" gate="G$1" x="63.5" y="15.24" rot="MR180"/>
<instance part="IC4" gate="G$1" x="111.76" y="35.56"/>
<instance part="QF1" gate="G$1" x="93.98" y="30.48"/>
<instance part="XF1" gate="G$1" x="114.3" y="7.62"/>
<instance part="L7" gate="G$1" x="83.82" y="27.94"/>
<instance part="C6" gate="G$1" x="5.08" y="30.48"/>
<instance part="C7" gate="G$1" x="-22.86" y="25.4"/>
<instance part="C8" gate="G$1" x="25.4" y="33.02" rot="R270"/>
<instance part="C9" gate="G$1" x="43.18" y="33.02"/>
<instance part="C10" gate="G$1" x="-17.78" y="20.32" rot="R90"/>
<instance part="C11" gate="G$1" x="35.56" y="15.24" rot="R270"/>
<instance part="SUPPLY1" gate="GND" x="30.48" y="68.58"/>
<instance part="SUPPLY2" gate="GND" x="-10.16" y="66.04"/>
<instance part="SUPPLY3" gate="GND" x="-30.48" y="104.14"/>
<instance part="SUPPLY4" gate="GND" x="35.56" y="93.98"/>
<instance part="SUPPLY6" gate="GND" x="58.42" y="66.04"/>
<instance part="SUPPLY7" gate="GND" x="71.12" y="66.04"/>
<instance part="SUPPLY8" gate="GND" x="86.36" y="55.88"/>
<instance part="SUPPLY9" gate="GND" x="162.56" y="83.82"/>
<instance part="SUPPLY10" gate="GND" x="200.66" y="68.58"/>
<instance part="SUPPLY11" gate="GND" x="111.76" y="27.94"/>
<instance part="SUPPLY12" gate="GND" x="114.3" y="0"/>
<instance part="SUPPLY13" gate="GND" x="93.98" y="2.54"/>
<instance part="R1" gate="G$1" x="35.56" y="73.66" rot="R90"/>
<instance part="R2" gate="G$1" x="111.76" y="73.66" rot="R90"/>
<instance part="R3" gate="G$1" x="149.86" y="86.36" rot="R270"/>
<instance part="R4" gate="G$1" x="187.96" y="78.74" rot="R90"/>
<instance part="R5" gate="G$1" x="-5.08" y="15.24"/>
<instance part="R6" gate="G$1" x="35.56" y="10.16" rot="R180"/>
<instance part="R7" gate="G$1" x="60.96" y="27.94"/>
<instance part="R8" gate="G$1" x="-15.24" y="15.24"/>
<instance part="C12" gate="G$1" x="-2.54" y="20.32" rot="R270"/>
<instance part="SUPPLY15" gate="GND" x="-10.16" y="7.62"/>
<instance part="R9" gate="G$1" x="-5.08" y="2.54"/>
<instance part="C13" gate="G$1" x="-2.54" y="10.16" rot="R270"/>
<instance part="C14" gate="G$1" x="63.5" y="25.4" rot="R270"/>
<instance part="C15" gate="G$1" x="71.12" y="27.94"/>
<instance part="SUPPLY16" gate="GND" x="55.88" y="22.86"/>
<instance part="R10" gate="G$1" x="50.8" y="38.1"/>
<instance part="R11" gate="G$1" x="50.8" y="12.7"/>
<instance part="C16" gate="G$1" x="76.2" y="5.08"/>
<instance part="R12" gate="G$1" x="35.56" y="5.08" rot="R180"/>
<instance part="R13" gate="G$1" x="53.34" y="5.08" rot="R180"/>
<instance part="SUPPLY14" gate="GND" x="27.94" y="0"/>
<instance part="SUPPLY17" gate="GND" x="76.2" y="-2.54"/>
<instance part="R14" gate="G$1" x="7.62" y="-5.08" rot="R90"/>
<instance part="R15" gate="G$1" x="83.82" y="-5.08" rot="R90"/>
<instance part="R16" gate="G$1" x="101.6" y="22.86" rot="R90"/>
<instance part="C17" gate="G$1" x="99.06" y="15.24" rot="R270"/>
<instance part="X1" gate="G$1" x="104.14" y="7.62"/>
<instance part="C18" gate="G$1" x="99.06" y="7.62" rot="R270"/>
<instance part="R17" gate="G$1" x="129.54" y="-10.16"/>
<instance part="C19" gate="G$1" x="127" y="30.48"/>
<instance part="SUPPLY18" gate="GND" x="127" y="22.86"/>
<instance part="C20" gate="G$1" x="109.22" y="-15.24"/>
<instance part="L8" gate="G$1" x="106.68" y="0"/>
<instance part="L9" gate="G$1" x="109.22" y="0"/>
<instance part="R18" gate="G$1" x="111.76" y="0" rot="R90"/>
<instance part="C21" gate="G$1" x="132.08" y="12.7"/>
<instance part="C22" gate="G$1" x="134.62" y="33.02" rot="R90"/>
<instance part="SUPPLY19" gate="GND" x="132.08" y="5.08"/>
<instance part="U$1" gate="A" x="78.74" y="-33.02" rot="MR0"/>
<instance part="R19" gate="G$1" x="81.28" y="-40.64" rot="R180"/>
<instance part="R20" gate="G$1" x="93.98" y="-35.56" rot="R180"/>
<instance part="U$1" gate="B" x="111.76" y="-35.56" rot="R180"/>
<instance part="R21" gate="G$1" x="58.42" y="-33.02" rot="R180"/>
<instance part="R22" gate="G$1" x="81.28" y="-20.32" rot="R180"/>
<instance part="R23" gate="G$1" x="93.98" y="-20.32" rot="R180"/>
<instance part="SUPPLY20" gate="GND" x="76.2" y="-25.4"/>
<instance part="C23" gate="G$1" x="-17.78" y="10.16" rot="R90"/>
<instance part="C24" gate="G$1" x="116.84" y="91.44"/>
<instance part="C25" gate="G$1" x="106.68" y="76.2"/>
<instance part="C26" gate="G$1" x="88.9" y="86.36" rot="R90"/>
<instance part="C27" gate="G$1" x="175.26" y="22.86" rot="R90"/>
<instance part="C28" gate="G$1" x="162.56" y="35.56" rot="R90"/>
<instance part="C29" gate="G$1" x="160.02" y="12.7"/>
<instance part="SUPPLY21" gate="GND" x="160.02" y="5.08"/>
<instance part="SUPPLY22" gate="GND" x="154.94" y="12.7"/>
<instance part="C30" gate="G$1" x="180.34" y="38.1"/>
<instance part="C31" gate="G$1" x="172.72" y="38.1"/>
<instance part="SUPPLY23" gate="GND" x="180.34" y="30.48"/>
<instance part="SUPPLY24" gate="GND" x="172.72" y="30.48"/>
<instance part="SUPPLY25" gate="GND" x="149.86" y="12.7"/>
<instance part="C32" gate="G$1" x="116.84" y="-15.24"/>
<instance part="SUPPLY26" gate="GND" x="109.22" y="-22.86"/>
<instance part="SUPPLY27" gate="GND" x="116.84" y="-22.86"/>
<instance part="J1" gate="G$1" x="274.32" y="20.32"/>
<instance part="U$2" gate="G$1" x="-88.9" y="101.6"/>
<instance part="J2" gate="G$1" x="-83.82" y="83.82" rot="MR0"/>
<instance part="VR4" gate="G$1" x="269.24" y="0"/>
<instance part="VR4" gate="G$2" x="142.24" y="25.4" rot="R90"/>
<instance part="SUPPLY28" gate="GND" x="142.24" y="17.78"/>
<instance part="SUPPLY29" gate="GND" x="269.24" y="7.62"/>
<instance part="SUPPLY30" gate="GND" x="271.78" y="-10.16"/>
<instance part="CON1" gate="_SIGNAL" x="284.48" y="12.7"/>
<instance part="CON1" gate="_GND" x="284.48" y="10.16"/>
<instance part="C33" gate="G$1" x="38.1" y="78.74" rot="R90"/>
<instance part="R24" gate="G$1" x="91.44" y="71.12" rot="R180"/>
<instance part="R25" gate="G$1" x="116.84" y="63.5"/>
<instance part="C34" gate="G$1" x="124.46" y="68.58" rot="R270"/>
<instance part="SUPPLY31" gate="GND" x="106.68" y="66.04"/>
<instance part="R26" gate="G$1" x="101.6" y="63.5" rot="R180"/>
<instance part="C35" gate="G$1" x="101.6" y="58.42" rot="R270"/>
<instance part="R27" gate="G$1" x="127" y="55.88" rot="R270"/>
<instance part="C36" gate="G$1" x="144.78" y="93.98" rot="R270"/>
<instance part="SUPPLY32" gate="GND" x="142.24" y="83.82"/>
<instance part="D1" gate="G$1" x="142.24" y="76.2" rot="R180"/>
<instance part="D2" gate="G$1" x="147.32" y="76.2" rot="R180"/>
<instance part="C37" gate="G$1" x="147.32" y="71.12" rot="R270"/>
<instance part="SUPPLY33" gate="GND" x="134.62" y="63.5"/>
<instance part="R28" gate="G$1" x="160.02" y="71.12"/>
<instance part="L10" gate="G$1" x="165.1" y="88.9"/>
<instance part="C38" gate="G$1" x="162.56" y="66.04" rot="R270"/>
<instance part="R29" gate="G$1" x="165.1" y="58.42" rot="R270"/>
<instance part="C39" gate="G$1" x="170.18" y="96.52"/>
<instance part="C40" gate="G$1" x="175.26" y="96.52"/>
<instance part="R30" gate="G$1" x="180.34" y="86.36" rot="R180"/>
<instance part="SUPPLY34" gate="GND" x="170.18" y="88.9"/>
<instance part="C41" gate="G$1" x="193.04" y="73.66" rot="R90"/>
<instance part="L11" gate="G$1" x="208.28" y="88.9"/>
<instance part="L12" gate="G$1" x="215.9" y="93.98" rot="R90"/>
<instance part="C42" gate="G$1" x="223.52" y="93.98" rot="R90"/>
<instance part="C43" gate="G$1" x="228.6" y="88.9"/>
<instance part="C44" gate="G$1" x="205.74" y="81.28" rot="R270"/>
<instance part="SUPPLY35" gate="GND" x="228.6" y="78.74"/>
<instance part="C45" gate="G$1" x="233.68" y="88.9"/>
<instance part="L13" gate="G$1" x="238.76" y="93.98" rot="R90"/>
<instance part="C46" gate="G$1" x="243.84" y="88.9"/>
<instance part="L14" gate="G$1" x="248.92" y="93.98" rot="R90"/>
<instance part="L15" gate="G$1" x="259.08" y="93.98" rot="R90"/>
<instance part="C47" gate="G$1" x="254" y="88.9"/>
<instance part="SUPPLY36" gate="GND" x="233.68" y="78.74"/>
<instance part="SUPPLY37" gate="GND" x="243.84" y="78.74"/>
<instance part="SUPPLY38" gate="GND" x="254" y="78.74"/>
<instance part="C48" gate="G$1" x="264.16" y="88.9"/>
<instance part="SUPPLY39" gate="GND" x="264.16" y="78.74"/>
<instance part="SUPPLY40" gate="GND" x="172.72" y="60.96"/>
<instance part="R31" gate="G$1" x="53.34" y="7.62" rot="R180"/>
<instance part="SUPPLY41" gate="GND" x="35.56" y="66.04"/>
<instance part="SUPPLY42" gate="GND" x="43.18" y="71.12"/>
<instance part="C49" gate="G$1" x="-15.24" y="76.2"/>
<instance part="SUPPLY43" gate="GND" x="-15.24" y="66.04"/>
<instance part="C50" gate="G$1" x="-48.26" y="111.76" rot="R180"/>
<instance part="C51" gate="G$1" x="-35.56" y="114.3"/>
<instance part="R32" gate="G$1" x="-45.72" y="111.76" rot="R90"/>
<instance part="SUPPLY44" gate="GND" x="-48.26" y="119.38" rot="R180"/>
<instance part="R33" gate="G$1" x="-71.12" y="114.3" rot="R90"/>
<instance part="SUPPLY45" gate="GND" x="-78.74" y="96.52"/>
<instance part="SUPPLY46" gate="GND" x="-78.74" y="76.2"/>
<instance part="C52" gate="G$1" x="-58.42" y="106.68" rot="R90"/>
<instance part="C53" gate="G$1" x="-66.04" y="58.42"/>
<instance part="R34" gate="G$1" x="-55.88" y="71.12" rot="R180"/>
<instance part="R35" gate="G$1" x="-66.04" y="66.04" rot="R270"/>
<instance part="R36" gate="G$1" x="-55.88" y="55.88"/>
<instance part="C54" gate="G$1" x="-45.72" y="60.96"/>
<instance part="C55" gate="G$1" x="-48.26" y="66.04" rot="R270"/>
<instance part="SUPPLY47" gate="GND" x="-66.04" y="50.8"/>
<instance part="SUPPLY48" gate="GND" x="-60.96" y="40.64"/>
<instance part="R37" gate="G$1" x="0" y="55.88"/>
<instance part="C56" gate="G$1" x="-20.32" y="55.88" rot="R90"/>
<instance part="R38" gate="G$1" x="-43.18" y="48.26"/>
<instance part="C57" gate="G$1" x="-33.02" y="48.26" rot="R90"/>
<instance part="C58" gate="G$1" x="-33.02" y="60.96" rot="R90"/>
<instance part="R39" gate="G$1" x="-38.1" y="55.88" rot="R90"/>
<instance part="SUPPLY5" gate="GND" x="-27.94" y="55.88"/>
<instance part="C59" gate="G$1" x="-53.34" y="48.26" rot="R90"/>
<instance part="SUPPLY49" gate="GND" x="15.24" y="48.26"/>
<instance part="C60" gate="G$1" x="-43.18" y="111.76" rot="R180"/>
<instance part="C61" gate="G$1" x="-12.7" y="83.82" rot="R90"/>
<instance part="R40" gate="G$1" x="-12.7" y="104.14" rot="R90"/>
<instance part="R41" gate="G$1" x="-5.08" y="109.22" rot="R180"/>
<instance part="R42" gate="G$1" x="2.54" y="104.14" rot="R270"/>
<instance part="SUPPLY51" gate="GND" x="-12.7" y="96.52"/>
<instance part="SUPPLY52" gate="GND" x="2.54" y="96.52"/>
<instance part="C62" gate="G$1" x="45.72" y="0"/>
<instance part="SUPPLY53" gate="GND" x="45.72" y="-17.78"/>
<instance part="RELAY1" gate="G$1" x="264.16" y="53.34" rot="MR180"/>
<instance part="C63" gate="G$1" x="248.92" y="71.12" rot="R90"/>
<instance part="C64" gate="G$1" x="233.68" y="45.72" rot="R90"/>
<instance part="SUPPLY55" gate="GND" x="-38.1" y="20.32"/>
<instance part="D3" gate="G$1" x="228.6" y="58.42" rot="R90"/>
<instance part="SUPPLY56" gate="GND" x="228.6" y="53.34"/>
<instance part="SUPPLY57" gate="GND" x="264.16" y="40.64"/>
<instance part="X2" gate="G$1" x="284.48" y="43.18" rot="MR0"/>
<instance part="SUPPLY58" gate="GND" x="281.94" y="38.1"/>
<instance part="TR1" gate="G$1" x="190.5" y="5.08"/>
<instance part="TR2" gate="G$1" x="205.74" y="5.08"/>
<instance part="R43" gate="G$1" x="193.04" y="15.24" rot="R90"/>
<instance part="R44" gate="G$1" x="208.28" y="30.48" rot="R90"/>
<instance part="R45" gate="G$1" x="198.12" y="33.02" rot="R180"/>
<instance part="R46" gate="G$1" x="213.36" y="53.34"/>
<instance part="IC5" gate="G$1" x="177.8" y="-33.02"/>
<instance part="C65" gate="G$1" x="167.64" y="-38.1"/>
<instance part="C66" gate="G$1" x="187.96" y="-38.1"/>
<instance part="SUPPLY59" gate="GND" x="167.64" y="-45.72"/>
<instance part="SUPPLY60" gate="GND" x="177.8" y="-38.1"/>
<instance part="SUPPLY61" gate="GND" x="187.96" y="-45.72"/>
<instance part="IC6" gate="G$1" x="154.94" y="-12.7" rot="MR0"/>
<instance part="C67" gate="G$1" x="142.24" y="-15.24"/>
<instance part="SUPPLY62" gate="GND" x="142.24" y="-22.86"/>
<instance part="SUPPLY63" gate="GND" x="154.94" y="-17.78"/>
<instance part="SUPPLY64" gate="GND" x="193.04" y="-2.54"/>
<instance part="SUPPLY65" gate="GND" x="208.28" y="-2.54"/>
<instance part="SUPPLY66" gate="GND" x="162.56" y="-35.56"/>
<instance part="SW1" gate="G$1" x="-83.82" y="53.34" rot="R270"/>
<instance part="SUPPLY68" gate="GND" x="-88.9" y="45.72"/>
<instance part="CON2" gate="PTT" x="-81.28" y="68.58" rot="R180"/>
<instance part="CON2" gate="VAGC" x="106.68" y="-45.72"/>
<instance part="CON2" gate="+12V" x="160.02" y="-30.48" rot="R180"/>
<instance part="CON2" gate="PWRSW" x="281.94" y="5.08"/>
<instance part="CON2" gate="TXEN" x="281.94" y="-20.32"/>
<instance part="CON2" gate="RXEN" x="281.94" y="-22.86"/>
<instance part="CON2" gate="GND" x="160.02" y="-33.02" rot="R180"/>
<instance part="CON3" gate="_SIGNAL" x="12.7" y="109.22"/>
<instance part="CON3" gate="_GND" x="12.7" y="106.68"/>
<instance part="CON4" gate="_SIGNAL" x="40.64" y="-12.7" rot="R180"/>
<instance part="CON4" gate="_GND" x="40.64" y="-15.24" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="5"/>
<pinref part="C8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="L5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="3"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="38.1" y1="35.56" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="G2"/>
<wire x1="43.18" y1="35.56" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<junction x="43.18" y="35.56"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="3"/>
<wire x1="10.16" y1="35.56" x2="5.08" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="5.08" y1="35.56" x2="5.08" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="35.56" y1="96.52" x2="35.56" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="58.42" y1="71.12" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="3"/>
<wire x1="53.34" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<junction x="58.42" y="71.12"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="71.12" y1="71.12" x2="71.12" y2="68.58" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="3"/>
<wire x1="76.2" y1="71.12" x2="71.12" y2="71.12" width="0.1524" layer="91"/>
<junction x="71.12" y="71.12"/>
</segment>
<segment>
<pinref part="Q5" gate="G$1" pin="S"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="162.56" y1="93.98" x2="162.56" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="200.66" y1="83.82" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="200.66" y1="81.28" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="200.66" y1="73.66" x2="200.66" y2="71.12" width="0.1524" layer="91"/>
<wire x1="198.12" y1="73.66" x2="200.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="200.66" y="73.66"/>
<pinref part="C44" gate="G$1" pin="2"/>
<junction x="200.66" y="81.28"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="111.76" y1="33.02" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XF1" gate="G$1" pin="COM"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="114.3" y1="2.54" x2="114.3" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="QF1" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="93.98" y1="25.4" x2="93.98" y2="15.24" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="5"/>
<wire x1="93.98" y1="15.24" x2="93.98" y2="7.62" width="0.1524" layer="91"/>
<wire x1="93.98" y1="7.62" x2="93.98" y2="5.08" width="0.1524" layer="91"/>
<wire x1="88.9" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<junction x="93.98" y="25.4"/>
<pinref part="C17" gate="G$1" pin="2"/>
<junction x="93.98" y="15.24"/>
<pinref part="C18" gate="G$1" pin="A"/>
<junction x="93.98" y="7.62"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="20.32" x2="-10.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="20.32" x2="-7.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="20.32" width="0.1524" layer="91"/>
<junction x="-10.16" y="15.24"/>
<junction x="-10.16" y="20.32"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="10.16" x2="-10.16" y2="10.16" width="0.1524" layer="91"/>
<junction x="-10.16" y="10.16"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="10.16" x2="-10.16" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<wire x1="58.42" y1="25.4" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="55.88" y1="27.94" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<junction x="55.88" y="25.4"/>
</segment>
<segment>
<pinref part="L4" gate="G$1" pin="4"/>
<pinref part="L5" gate="G$1" pin="4"/>
<wire x1="20.32" y1="27.94" x2="27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<junction x="27.94" y="27.94"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="27.94" y1="15.24" x2="27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="30.48" y1="15.24" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<junction x="27.94" y="15.24"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="30.48" y1="10.16" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<wire x1="27.94" y1="2.54" x2="27.94" y2="5.08" width="0.1524" layer="91"/>
<junction x="27.94" y="10.16"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="27.94" y1="5.08" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="30.48" y1="5.08" x2="27.94" y2="5.08" width="0.1524" layer="91"/>
<junction x="27.94" y="5.08"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="-"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="76.2" y1="-22.86" x2="76.2" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="-"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="-IN"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="149.86" y1="20.32" x2="149.86" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="-"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="VR4" gate="G$2" pin="1"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SLEEVE"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="269.24" y1="10.16" x2="269.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="CON1" gate="_GND" pin="S"/>
<wire x1="279.4" y1="10.16" x2="269.24" y2="10.16" width="0.1524" layer="91"/>
<junction x="269.24" y="10.16"/>
</segment>
<segment>
<pinref part="VR4" gate="G$1" pin="P"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="271.78" y1="-5.08" x2="271.78" y2="-7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="30.48" y1="73.66" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="119.38" y1="68.58" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<wire x1="111.76" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<junction x="111.76" y="68.58"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="106.68" y1="71.12" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<junction x="106.68" y="68.58"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="5"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="86.36" y1="73.66" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="86.36" y1="71.12" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<junction x="86.36" y="71.12"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="96.52" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<wire x1="88.9" y1="58.42" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<junction x="86.36" y="58.42"/>
</segment>
<segment>
<pinref part="L3" gate="G$1" pin="5"/>
<wire x1="139.7" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="142.24" y1="88.9" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="139.7" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="134.62" y1="76.2" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="134.62" y1="71.12" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="142.24" y1="71.12" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<junction x="134.62" y="71.12"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="157.48" y1="66.04" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<junction x="134.62" y="66.04"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="A"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="A"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<wire x1="228.6" y1="83.82" x2="228.6" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<wire x1="233.68" y1="83.82" x2="233.68" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<wire x1="243.84" y1="83.82" x2="243.84" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="254" y1="83.82" x2="254" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<wire x1="264.16" y1="83.82" x2="264.16" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VR1" gate="G$1" pin="2"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<wire x1="172.72" y1="63.5" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="5"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC2" gate="A" pin="GND"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="-38.1" y1="106.68" x2="-35.56" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="-35.56" y1="106.68" x2="-30.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="109.22" x2="-35.56" y2="106.68" width="0.1524" layer="91"/>
<junction x="-35.56" y="106.68"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="68.58" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="68.58" x2="-15.24" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="116.84" x2="-48.26" y2="116.84" width="0.1524" layer="91"/>
<junction x="-48.26" y="116.84"/>
<pinref part="IC2" gate="A" pin="GAINSW"/>
<wire x1="-50.8" y1="106.68" x2="-50.8" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="116.84" x2="-48.26" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="116.84" x2="-43.18" y2="116.84" width="0.1524" layer="91"/>
<junction x="-45.72" y="116.84"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="-"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SLEEVE"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="VR2" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C58" gate="G$1" pin="2"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="-27.94" y1="60.96" x2="-27.94" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VR3" gate="G$1" pin="2"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<wire x1="2.54" y1="99.06" x2="7.62" y2="99.06" width="0.1524" layer="91"/>
<wire x1="7.62" y1="99.06" x2="7.62" y2="106.68" width="0.1524" layer="91"/>
<junction x="2.54" y="99.06"/>
<pinref part="CON3" gate="_GND" pin="S"/>
</segment>
<segment>
<pinref part="L6" gate="G$1" pin="5"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="RELAY1" gate="G$1" pin="L1"/>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="G"/>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C66" gate="G$1" pin="2"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="2"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="G"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR1" gate="G$1" pin="E"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="TR2" gate="G$1" pin="E"/>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="P"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
<wire x1="-88.9" y1="50.8" x2="-88.9" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
<pinref part="CON2" gate="GND" pin="S"/>
</segment>
<segment>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
<pinref part="CON4" gate="_GND" pin="S"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="30.48" x2="-22.86" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="27.94" x2="-22.86" y2="30.48" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="G2"/>
<wire x1="-22.86" y1="30.48" x2="-10.16" y2="30.48" width="0.1524" layer="91"/>
<junction x="-22.86" y="30.48"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="L6" gate="G$1" pin="3"/>
<wire x1="-27.94" y1="20.32" x2="-22.86" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="20.32" x2="-22.86" y2="20.32" width="0.1524" layer="91"/>
<junction x="-22.86" y="20.32"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="15.24" x2="-22.86" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="15.24" x2="-22.86" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="2.54" x2="-22.86" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="2.54" x2="-22.86" y2="15.24" width="0.1524" layer="91"/>
<junction x="-22.86" y="15.24"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="SOURCE"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="0" y1="25.4" x2="0" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="0" y1="20.32" x2="0" y2="15.24" width="0.1524" layer="91"/>
<junction x="0" y="20.32"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="0" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="5.08" y1="25.4" x2="7.62" y2="25.4" width="0.1524" layer="91"/>
<wire x1="7.62" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="10.16" width="0.1524" layer="91"/>
<junction x="7.62" y="25.4"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="25.4" width="0.1524" layer="91"/>
<wire x1="0" y1="10.16" x2="7.62" y2="10.16" width="0.1524" layer="91"/>
<junction x="7.62" y="10.16"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="7.62" y1="0" x2="7.62" y2="2.54" width="0.1524" layer="91"/>
<junction x="7.62" y="2.54"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="10.16" y1="30.48" x2="7.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="38.1" width="0.1524" layer="91"/>
<wire x1="7.62" y1="38.1" x2="0" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="DRAIN"/>
<wire x1="0" y1="38.1" x2="0" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="38.1" y1="30.48" x2="40.64" y2="30.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="30.48" x2="40.64" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="40.64" y1="15.24" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="40.64" y1="10.16" x2="40.64" y2="15.24" width="0.1524" layer="91"/>
<junction x="40.64" y="15.24"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="48.26" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="10.16" width="0.1524" layer="91"/>
<junction x="40.64" y="10.16"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="43.18" y1="25.4" x2="43.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="38.1" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<pinref part="Q8" gate="G$1" pin="G2"/>
<wire x1="55.88" y1="15.24" x2="48.26" y2="15.24" width="0.1524" layer="91"/>
<wire x1="48.26" y1="15.24" x2="48.26" y2="25.4" width="0.1524" layer="91"/>
<wire x1="48.26" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<junction x="43.18" y="25.4"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="Q7" gate="G$1" pin="SOURCE"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="66.04" y1="30.48" x2="66.04" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="66.04" y1="27.94" x2="66.04" y2="25.4" width="0.1524" layer="91"/>
<junction x="66.04" y="27.94"/>
<pinref part="Q8" gate="G$1" pin="SOURCE"/>
<wire x1="66.04" y1="25.4" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
<junction x="66.04" y="25.4"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="Q7" gate="G$1" pin="G1"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="G1"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="45.72" y1="38.1" x2="45.72" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="40.64" y1="5.08" x2="45.72" y2="5.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="5.08" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="45.72" y1="12.7" x2="45.72" y2="5.08" width="0.1524" layer="91"/>
<junction x="45.72" y="12.7"/>
<junction x="45.72" y="5.08"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="45.72" y1="2.54" x2="45.72" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="1"/>
<wire x1="71.12" y1="33.02" x2="78.74" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="71.12" y1="30.48" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<pinref part="Q7" gate="G$1" pin="DRAIN"/>
<wire x1="66.04" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<wire x1="71.12" y1="40.64" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<junction x="71.12" y="33.02"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="3"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="71.12" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<pinref part="Q8" gate="G$1" pin="DRAIN"/>
<wire x1="66.04" y1="10.16" x2="71.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="10.16" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<junction x="71.12" y="22.86"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="2"/>
<wire x1="78.74" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="27.94" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="58.42" y1="5.08" x2="68.58" y2="5.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="5.08" x2="68.58" y2="7.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="7.62" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<junction x="76.2" y="7.62"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="83.82" y1="0" x2="83.82" y2="7.62" width="0.1524" layer="91"/>
<wire x1="83.82" y1="7.62" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="58.42" y1="7.62" x2="68.58" y2="7.62" width="0.1524" layer="91"/>
<junction x="68.58" y="7.62"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="4"/>
<pinref part="QF1" gate="G$1" pin="1"/>
<wire x1="88.9" y1="30.48" x2="91.44" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="QF1" gate="G$1" pin="3"/>
<wire x1="96.52" y1="30.48" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="RFIN"/>
<wire x1="101.6" y1="30.48" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="101.6" y1="27.94" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<junction x="101.6" y="30.48"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="101.6" y1="17.78" x2="101.6" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="RFBIAS"/>
<wire x1="104.14" y1="33.02" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="15.24" x2="101.6" y2="15.24" width="0.1524" layer="91"/>
<junction x="101.6" y="15.24"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<pinref part="C18" gate="G$1" pin="E"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="OSC"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="106.68" y1="33.02" x2="106.68" y2="7.62" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="106.68" y1="5.08" x2="106.68" y2="7.62" width="0.1524" layer="91"/>
<junction x="106.68" y="7.62"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="IFIN"/>
<wire x1="116.84" y1="33.02" x2="116.84" y2="15.24" width="0.1524" layer="91"/>
<wire x1="116.84" y1="15.24" x2="119.38" y2="15.24" width="0.1524" layer="91"/>
<pinref part="XF1" gate="G$1" pin="OUT"/>
<wire x1="119.38" y1="15.24" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="MIXOUT"/>
<pinref part="XF1" gate="G$1" pin="IN"/>
<wire x1="109.22" y1="33.02" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="1"/>
<wire x1="109.22" y1="5.08" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
<junction x="109.22" y="12.7"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="111.76" y1="5.08" x2="109.22" y2="5.08" width="0.1524" layer="91"/>
<junction x="109.22" y="5.08"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="121.92" y1="20.32" x2="119.38" y2="20.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="20.32" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-5.08" x2="111.76" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="L9" gate="G$1" pin="2"/>
<junction x="109.22" y="-5.08"/>
<pinref part="L8" gate="G$1" pin="2"/>
<wire x1="106.68" y1="-5.08" x2="109.22" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-5.08" x2="109.22" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-10.16" x2="109.22" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="99.06" y1="-20.32" x2="99.06" y2="-10.16" width="0.1524" layer="91"/>
<junction x="99.06" y="-10.16"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-10.16" x2="99.06" y2="-10.16" width="0.1524" layer="91"/>
<junction x="83.82" y="-10.16"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="7.62" y1="-10.16" x2="83.82" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-10.16" x2="116.84" y2="-10.16" width="0.1524" layer="91"/>
<junction x="109.22" y="-10.16"/>
<pinref part="C32" gate="G$1" pin="+"/>
<wire x1="116.84" y1="-10.16" x2="121.92" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-10.16" x2="124.46" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-12.7" x2="116.84" y2="-10.16" width="0.1524" layer="91"/>
<junction x="116.84" y="-10.16"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-12.7" x2="109.22" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="20.32" x2="121.92" y2="-10.16" width="0.1524" layer="91"/>
<junction x="121.92" y="-10.16"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="AFOUT"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="121.92" y1="33.02" x2="127" y2="33.02" width="0.1524" layer="91"/>
<wire x1="127" y1="33.02" x2="129.54" y2="33.02" width="0.1524" layer="91"/>
<junction x="127" y="33.02"/>
<pinref part="C22" gate="G$1" pin="+"/>
<wire x1="129.54" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="+"/>
<wire x1="132.08" y1="17.78" x2="132.08" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="VAGC"/>
<wire x1="114.3" y1="33.02" x2="114.3" y2="17.78" width="0.1524" layer="91"/>
<wire x1="114.3" y1="17.78" x2="132.08" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="B" pin="+IN"/>
<wire x1="119.38" y1="-38.1" x2="137.16" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="137.16" y1="-38.1" x2="137.16" y2="17.78" width="0.1524" layer="91"/>
<wire x1="137.16" y1="17.78" x2="132.08" y2="17.78" width="0.1524" layer="91"/>
<junction x="132.08" y="17.78"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="86.36" y1="-40.64" x2="88.9" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-40.64" x2="88.9" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U$1" gate="A" pin="-IN"/>
<wire x1="86.36" y1="-35.56" x2="88.9" y2="-35.56" width="0.1524" layer="91"/>
<junction x="88.9" y="-35.56"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$1" gate="B" pin="OUT"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="104.14" y1="-35.56" x2="101.6" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-35.56" x2="99.06" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-35.56" x2="101.6" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-27.94" x2="121.92" y2="-27.94" width="0.1524" layer="91"/>
<junction x="101.6" y="-35.56"/>
<wire x1="121.92" y1="-27.94" x2="121.92" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U$1" gate="B" pin="-IN"/>
<wire x1="121.92" y1="-33.02" x2="119.38" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-45.72" x2="101.6" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-45.72" x2="101.6" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="CON2" gate="VAGC" pin="S"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="76.2" y1="-40.64" x2="68.58" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-40.64" x2="68.58" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U$1" gate="A" pin="OUT"/>
<wire x1="68.58" y1="-33.02" x2="71.12" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="63.5" y1="-33.02" x2="68.58" y2="-33.02" width="0.1524" layer="91"/>
<junction x="68.58" y="-33.02"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="53.34" y1="-33.02" x2="-43.18" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="-33.02" x2="-43.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="G1"/>
<wire x1="-43.18" y1="10.16" x2="-43.18" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="33.02" x2="-10.16" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="10.16" x2="-43.18" y2="10.16" width="0.1524" layer="91"/>
<junction x="-43.18" y="10.16"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U$1" gate="A" pin="+IN"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="86.36" y1="-30.48" x2="86.36" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-20.32" x2="86.36" y2="-20.32" width="0.1524" layer="91"/>
<junction x="86.36" y="-20.32"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GAIN@1"/>
<pinref part="C28" gate="G$1" pin="+"/>
<wire x1="160.02" y1="27.94" x2="160.02" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="GAIN@2"/>
<wire x1="162.56" y1="27.94" x2="167.64" y2="27.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="27.94" x2="167.64" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="-"/>
<wire x1="167.64" y1="33.02" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="OUT"/>
<pinref part="C27" gate="G$1" pin="+"/>
<wire x1="167.64" y1="22.86" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="BYPASS"/>
<wire x1="160.02" y1="15.24" x2="160.02" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="172.72" y1="40.64" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VS"/>
<wire x1="154.94" y1="40.64" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="+"/>
<wire x1="180.34" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="40.64"/>
<pinref part="Q4" gate="G$1" pin="C"/>
<wire x1="187.96" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<junction x="180.34" y="40.64"/>
<wire x1="185.42" y1="40.64" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-10.16" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<junction x="185.42" y="40.64"/>
<pinref part="IC6" gate="G$1" pin="I"/>
<wire x1="162.56" y1="-10.16" x2="185.42" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="VR4" gate="G$2" pin="BR"/>
<pinref part="IC3" gate="G$1" pin="+IN"/>
<wire x1="147.32" y1="25.4" x2="149.86" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="C22" gate="G$1" pin="-"/>
<pinref part="VR4" gate="G$2" pin="2"/>
<wire x1="139.7" y1="33.02" x2="142.24" y2="33.02" width="0.1524" layer="91"/>
<wire x1="142.24" y1="33.02" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="-"/>
<pinref part="J1" gate="G$1" pin="TIP"/>
<wire x1="180.34" y1="22.86" x2="269.24" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="CON1" gate="_SIGNAL" pin="S"/>
<wire x1="279.4" y1="12.7" x2="266.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="266.7" y1="12.7" x2="266.7" y2="20.32" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="SW"/>
<wire x1="266.7" y1="20.32" x2="269.24" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="VR4" gate="G$1" pin="S"/>
<wire x1="279.4" y1="5.08" x2="274.32" y2="5.08" width="0.1524" layer="91"/>
<pinref part="CON2" gate="PWRSW" pin="S"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="4"/>
<pinref part="C33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="OUT2"/>
<wire x1="30.48" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="35.56" y="78.74"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="53.34" y1="81.28" x2="58.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="58.42" y1="81.28" x2="60.96" y2="81.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="81.28" width="0.1524" layer="91"/>
<junction x="58.42" y="81.28"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="68.58" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="81.28" x2="76.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="78.74" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<junction x="71.12" y="81.28"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="4"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="86.36" y1="86.36" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="SOURCE"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="111.76" y1="78.74" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="106.68" y1="78.74" x2="111.76" y2="78.74" width="0.1524" layer="91"/>
<junction x="111.76" y="78.74"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="DRAIN"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="111.76" y1="91.44" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="129.54" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="116.84" y1="96.52" x2="116.84" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="3"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="129.54" y1="86.36" x2="127" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="127" y1="86.36" x2="116.84" y2="86.36" width="0.1524" layer="91"/>
<wire x1="127" y1="68.58" x2="127" y2="86.36" width="0.1524" layer="91"/>
<junction x="127" y="86.36"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="121.92" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="127" y2="68.58" width="0.1524" layer="91"/>
<junction x="127" y="68.58"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="127" y1="60.96" x2="127" y2="63.5" width="0.1524" layer="91"/>
<junction x="127" y="63.5"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="G1"/>
<wire x1="101.6" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="99.06" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="106.68" y1="63.5" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<junction x="111.76" y="63.5"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="104.14" y1="58.42" x2="111.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="58.42" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="96.52" y1="63.5" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="Q2" gate="G$1" pin="G2"/>
<wire x1="93.98" y1="86.36" x2="96.52" y2="86.36" width="0.1524" layer="91"/>
<wire x1="96.52" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="96.52" y1="71.12" x2="96.52" y2="86.36" width="0.1524" layer="91"/>
<junction x="96.52" y="71.12"/>
<junction x="96.52" y="86.36"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="4"/>
<pinref part="C36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="Q5" gate="G$1" pin="G"/>
<wire x1="152.4" y1="93.98" x2="149.86" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="149.86" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<wire x1="149.86" y1="91.44" x2="149.86" y2="93.98" width="0.1524" layer="91"/>
<junction x="149.86" y="93.98"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="VR1" gate="G$1" pin="BR"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="187.96" y1="68.58" x2="187.96" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="190.5" y1="73.66" x2="187.96" y2="73.66" width="0.1524" layer="91"/>
<junction x="187.96" y="73.66"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="D2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="149.86" y1="81.28" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="71.12" width="0.1524" layer="91"/>
<junction x="149.86" y="76.2"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="154.94" y1="71.12" x2="149.86" y2="71.12" width="0.1524" layer="91"/>
<junction x="149.86" y="71.12"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="L10" gate="G$1" pin="1"/>
<wire x1="165.1" y1="93.98" x2="165.1" y2="99.06" width="0.1524" layer="91"/>
<pinref part="Q5" gate="G$1" pin="D"/>
<wire x1="165.1" y1="99.06" x2="162.56" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="E"/>
<wire x1="165.1" y1="99.06" x2="170.18" y2="99.06" width="0.1524" layer="91"/>
<junction x="165.1" y="99.06"/>
<wire x1="170.18" y1="99.06" x2="175.26" y2="99.06" width="0.1524" layer="91"/>
<junction x="170.18" y="99.06"/>
<pinref part="C40" gate="G$1" pin="E"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="L10" gate="G$1" pin="2"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="165.1" y1="83.82" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="165.1" y1="66.04" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<junction x="165.1" y="71.12"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="165.1" y1="63.5" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
<junction x="165.1" y="66.04"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="195.58" y1="86.36" x2="187.96" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="187.96" y1="86.36" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="187.96" y1="83.82" x2="187.96" y2="86.36" width="0.1524" layer="91"/>
<junction x="187.96" y="86.36"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="C40" gate="G$1" pin="A"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="175.26" y1="91.44" x2="175.26" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<pinref part="L11" gate="G$1" pin="1"/>
<wire x1="200.66" y1="93.98" x2="208.28" y2="93.98" width="0.1524" layer="91"/>
<pinref part="L12" gate="G$1" pin="1"/>
<junction x="208.28" y="93.98"/>
<wire x1="208.28" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="L12" gate="G$1" pin="2"/>
<pinref part="C42" gate="G$1" pin="E"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="C45" gate="G$1" pin="1"/>
<pinref part="L13" gate="G$1" pin="1"/>
<wire x1="233.68" y1="91.44" x2="233.68" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="E"/>
<pinref part="C42" gate="G$1" pin="A"/>
<wire x1="228.6" y1="91.44" x2="228.6" y2="93.98" width="0.1524" layer="91"/>
<wire x1="233.68" y1="93.98" x2="228.6" y2="93.98" width="0.1524" layer="91"/>
<junction x="233.68" y="93.98"/>
<junction x="228.6" y="93.98"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="L13" gate="G$1" pin="2"/>
<pinref part="L14" gate="G$1" pin="1"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="243.84" y1="91.44" x2="243.84" y2="93.98" width="0.1524" layer="91"/>
<junction x="243.84" y="93.98"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="L14" gate="G$1" pin="2"/>
<pinref part="L15" gate="G$1" pin="1"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="254" y1="91.44" x2="254" y2="93.98" width="0.1524" layer="91"/>
<junction x="254" y="93.98"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<pinref part="L15" gate="G$1" pin="2"/>
<wire x1="264.16" y1="91.44" x2="264.16" y2="93.98" width="0.1524" layer="91"/>
<wire x1="264.16" y1="93.98" x2="269.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="269.24" y1="93.98" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
<junction x="264.16" y="93.98"/>
<pinref part="C63" gate="G$1" pin="2"/>
<wire x1="254" y1="71.12" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="BYPASS"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="78.74" x2="-15.24" y2="78.74" width="0.1524" layer="91"/>
<junction x="-10.16" y="78.74"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="C50" gate="G$1" pin="1"/>
<pinref part="IC2" gate="A" pin="INT"/>
<wire x1="-48.26" y1="109.22" x2="-48.26" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="V+" class="0">
<segment>
<pinref part="IC2" gate="A" pin="V+"/>
<wire x1="-40.64" y1="106.68" x2="-40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="-40.64" y1="116.84" x2="-35.56" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="127" y1="50.8" x2="165.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="165.1" y1="50.8" x2="165.1" y2="53.34" width="0.1524" layer="91"/>
<pinref part="VR1" gate="G$1" pin="1"/>
<wire x1="165.1" y1="50.8" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="193.04" y1="50.8" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<junction x="165.1" y="50.8"/>
<pinref part="Q3" gate="G$1" pin="C"/>
<wire x1="193.04" y1="58.42" x2="193.04" y2="63.5" width="0.1524" layer="91"/>
<junction x="193.04" y="58.42"/>
<wire x1="127" y1="50.8" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="30.48" y1="50.8" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<junction x="127" y="50.8"/>
<pinref part="VR3" gate="G$1" pin="1"/>
<wire x1="30.48" y1="60.96" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<wire x1="15.24" y1="60.96" x2="-17.78" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="60.96" x2="-17.78" y2="116.84" width="0.1524" layer="91"/>
<junction x="15.24" y="60.96"/>
<wire x1="-17.78" y1="116.84" x2="-35.56" y2="116.84" width="0.1524" layer="91"/>
<junction x="-35.56" y="116.84"/>
<pinref part="IC1" gate="G$1" pin="V+"/>
<wire x1="-17.78" y1="116.84" x2="30.48" y2="116.84" width="0.1524" layer="91"/>
<wire x1="30.48" y1="116.84" x2="30.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="-17.78" y="116.84"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="30.48" y1="106.68" x2="30.48" y2="88.9" width="0.1524" layer="91"/>
<wire x1="35.56" y1="106.68" x2="33.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="33.02" y1="106.68" x2="30.48" y2="106.68" width="0.1524" layer="91"/>
<junction x="30.48" y="106.68"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="-71.12" y1="119.38" x2="-71.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="124.46" x2="-17.78" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="124.46" x2="-17.78" y2="116.84" width="0.1524" layer="91"/>
<pinref part="RELAY1" gate="G$1" pin="L2"/>
<wire x1="193.04" y1="63.5" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="193.04" y="63.5"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="228.6" y1="63.5" x2="264.16" y2="63.5" width="0.1524" layer="91"/>
<wire x1="228.6" y1="60.96" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
<junction x="228.6" y="63.5"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="IC2" gate="A" pin="SENSE"/>
<pinref part="R32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="J2" gate="G$1" pin="RING"/>
<wire x1="-71.12" y1="109.22" x2="-71.12" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="106.68" x2="-71.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="88.9" x2="-78.74" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="-60.96" y1="106.68" x2="-71.12" y2="106.68" width="0.1524" layer="91"/>
<junction x="-71.12" y="106.68"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="RINGSW"/>
<wire x1="-78.74" y1="86.36" x2="-76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="86.36" x2="-76.2" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="+"/>
<wire x1="-76.2" y1="104.14" x2="-78.74" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="IC2" gate="A" pin="IN"/>
<pinref part="C52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="C53" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="IC2" gate="A" pin="GAIN2"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="76.2" x2="-50.8" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="IC2" gate="A" pin="GAIN1"/>
<wire x1="-53.34" y1="76.2" x2="-66.04" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-66.04" y1="76.2" x2="-66.04" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="71.12" x2="-66.04" y2="71.12" width="0.1524" layer="91"/>
<junction x="-66.04" y="71.12"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<pinref part="VR2" gate="G$1" pin="1"/>
<wire x1="-60.96" y1="53.34" x2="-60.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="-50.8" y1="55.88" x2="-45.72" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="C54" gate="G$1" pin="1"/>
<pinref part="IC2" gate="A" pin="OUT"/>
<wire x1="-45.72" y1="63.5" x2="-45.72" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="66.04" x2="-45.72" y2="76.2" width="0.1524" layer="91"/>
<junction x="-45.72" y="66.04"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="66.04" x2="-53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="68.58" x2="-48.26" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC2" gate="A" pin="DET"/>
<wire x1="-48.26" y1="68.58" x2="-48.26" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="VR3" gate="G$1" pin="BR"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="10.16" y1="55.88" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SIGNAL"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="73.66" x2="-5.08" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="55.88" x2="-5.08" y2="55.88" width="0.1524" layer="91"/>
<junction x="-5.08" y="55.88"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="IC2" gate="A" pin="BUFFGAIN"/>
<pinref part="IC2" gate="A" pin="BUFFOUT"/>
<wire x1="-40.64" y1="76.2" x2="-38.1" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="76.2" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="76.2" x2="-22.86" y2="55.88" width="0.1524" layer="91"/>
<junction x="-38.1" y="76.2"/>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="48.26" x2="-22.86" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="48.26" x2="-22.86" y2="55.88" width="0.1524" layer="91"/>
<junction x="-22.86" y="55.88"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="48.26" x2="-38.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="48.26" x2="-38.1" y2="48.26" width="0.1524" layer="91"/>
<junction x="-38.1" y="48.26"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="C58" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="60.96" x2="-35.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="60.96" x2="-38.1" y2="73.66" width="0.1524" layer="91"/>
<junction x="-38.1" y="60.96"/>
<wire x1="-38.1" y1="73.66" x2="-43.18" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IC2" gate="A" pin="BUFFIN"/>
<wire x1="-43.18" y1="73.66" x2="-43.18" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="VR2" gate="G$1" pin="BR"/>
<pinref part="C59" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="C59" gate="G$1" pin="2"/>
<pinref part="R38" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="-78.74" y1="68.58" x2="-73.66" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="TIP"/>
<wire x1="-78.74" y1="81.28" x2="-73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="81.28" x2="-73.66" y2="68.58" width="0.1524" layer="91"/>
<pinref part="CON2" gate="PTT" pin="S"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="IC2" gate="A" pin="VREF"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="106.68" x2="-43.18" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CARRIER"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="83.82" x2="-7.62" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="C61" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="83.82" x2="-15.24" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="109.22" x2="-12.7" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="109.22" x2="-10.16" y2="109.22" width="0.1524" layer="91"/>
<junction x="-12.7" y="109.22"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="0" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<wire x1="7.62" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<junction x="2.54" y="109.22"/>
<pinref part="CON3" gate="_SIGNAL" pin="S"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="C62" gate="G$1" pin="2"/>
<wire x1="45.72" y1="-5.08" x2="45.72" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="CON4" gate="_SIGNAL" pin="S"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="RELAY1" gate="G$1" pin="NC"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="243.84" y1="45.72" x2="238.76" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="C64" gate="G$1" pin="1"/>
<pinref part="L6" gate="G$1" pin="4"/>
<wire x1="231.14" y1="45.72" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="45.72" x2="-38.1" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="RELAY1" gate="G$1" pin="NO"/>
<wire x1="243.84" y1="60.96" x2="243.84" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="243.84" y1="71.12" x2="246.38" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="1"/>
<pinref part="RELAY1" gate="G$1" pin="COM1"/>
<wire x1="281.94" y1="43.18" x2="271.78" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="B"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="193.04" y1="20.32" x2="193.04" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="193.04" y1="33.02" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<junction x="193.04" y="33.02"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="B"/>
<wire x1="198.12" y1="53.34" x2="208.28" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="1"/>
<junction x="208.28" y="53.34"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="TR1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="TR2" gate="G$1" pin="C"/>
<wire x1="208.28" y1="25.4" x2="208.28" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="O" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="O"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-30.48" x2="187.96" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-30.48" x2="187.96" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="E"/>
<wire x1="198.12" y1="40.64" x2="203.2" y2="40.64" width="0.1524" layer="91"/>
<wire x1="203.2" y1="40.64" x2="218.44" y2="40.64" width="0.1524" layer="91"/>
<wire x1="218.44" y1="40.64" x2="218.44" y2="53.34" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="E"/>
<wire x1="218.44" y1="53.34" x2="218.44" y2="58.42" width="0.1524" layer="91"/>
<wire x1="218.44" y1="58.42" x2="203.2" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
<junction x="218.44" y="53.34"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="203.2" y1="33.02" x2="203.2" y2="40.64" width="0.1524" layer="91"/>
<junction x="203.2" y="40.64"/>
<wire x1="187.96" y1="-30.48" x2="218.44" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-30.48" x2="218.44" y2="40.64" width="0.1524" layer="91"/>
<junction x="187.96" y="-30.48"/>
<junction x="218.44" y="40.64"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="O"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="147.32" y1="-10.16" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="C67" gate="G$1" pin="1"/>
<wire x1="142.24" y1="-10.16" x2="134.62" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-12.7" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
<junction x="142.24" y="-10.16"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="C65" gate="G$1" pin="1"/>
<pinref part="IC5" gate="G$1" pin="I"/>
<wire x1="167.64" y1="-35.56" x2="167.64" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-30.48" x2="170.18" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-30.48" x2="167.64" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-25.4" x2="220.98" y2="-25.4" width="0.1524" layer="91"/>
<junction x="167.64" y="-30.48"/>
<pinref part="C44" gate="G$1" pin="1"/>
<pinref part="L11" gate="G$1" pin="2"/>
<wire x1="208.28" y1="81.28" x2="208.28" y2="83.82" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-25.4" x2="220.98" y2="81.28" width="0.1524" layer="91"/>
<wire x1="220.98" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="208.28" y="81.28"/>
<wire x1="162.56" y1="-30.48" x2="167.64" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="CON2" gate="+12V" pin="S"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="279.4" y1="-20.32" x2="200.66" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-20.32" x2="200.66" y2="5.08" width="0.1524" layer="91"/>
<pinref part="TR2" gate="G$1" pin="B"/>
<wire x1="200.66" y1="5.08" x2="203.2" y2="5.08" width="0.1524" layer="91"/>
<pinref part="CON2" gate="TXEN" pin="S"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="TR1" gate="G$1" pin="B"/>
<wire x1="279.4" y1="-22.86" x2="187.96" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-22.86" x2="187.96" y2="5.08" width="0.1524" layer="91"/>
<pinref part="CON2" gate="RXEN" pin="S"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="SW1" gate="G$1" pin="S"/>
<wire x1="-78.74" y1="48.26" x2="-71.12" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="48.26" x2="-71.12" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="TIPSW"/>
<wire x1="-71.12" y1="83.82" x2="-78.74" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
